<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';
class PDFDocumentacion extends TCPDF
{
  public $Titulo="";
  public $Titulosub="";
  public $Titulosub2="";
  public $CurrentDate="";
  public $underscore="";
  function __construct()
  {
    parent::__construct();
    $this->SetAuthor('Fernando Manuel Avila Cataño');
    $this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $this->SetMargins(15,20, 15);
    $this->SetHeaderMargin(PDF_MARGIN_HEADER);
    $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $this->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $this->setFontSubsetting(true);
    $this->SetFont('times', '', 10);
    $this->underscore="----------------------------------------------------------------------";
  }
  public function EncabezadoEdit($Titulo,$Titulosub,$Titulosub2,$CurrentDate)
  {
    $this->Titulo=$Titulo;
    $this->Titulosub=$Titulosub;
    $this->Titulosub2=$Titulosub2;
    $this->CurrentDate=$CurrentDate;
  }
  public function Header() {
  }
  public function Footer() {

  }
}
