<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Proveedores extends CI_Controller {
	public $filename="_proveedores";
	function __construct() {
		parent::__construct();
		$this->load->helper(
			array(
				'url',
				'form',
				'headers',
				'lotes',
			)
		);
		$this->load->model(
			array(
				'lotes/Proveedor',
				'Estados',
				'Contribuyentes',
			)
		);
	}
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function Cargar($value='')
	{
		HeaderJson();
		ini_set('post_max_size', '100M');
		ini_set('upload_max_filesize', '100M');
		if (!is_dir("./data/temp_data/")) {
			mkdir("./data/temp_data/", 0777, TRUE);
		}
		$config['upload_path'] = './data/temp_data/';
		$config['allowed_types'] = 'xlsx|csv';
		$config['overwrite'] = TRUE;
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload("archivo"))
		{
			$error = array('error' => $this->upload->display_errors());
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$fa=$this->upload->data();
			rename($fa['full_path'],$fa['file_path'].'archivo_temporal'.$this->filename.$fa["file_ext"]);
			$this->ReadExcel($fa["file_ext"]);
		}
	}
	private	function ReadExcel($extensionfile)
	{
		ini_set("max_execution_time", "-1");
		ini_set("memory_limit", "-1");
		ignore_user_abort(true);
		set_time_limit(0);
		$estadistic["start"] = microtime(true);
		$this->load->helper("file");
		$result=false;
		if ($extensionfile==".csv") {
			$this->load->library('csvreader');
			$result = $this->csvreader->parse_file('./data/temp_data/archivo_temporal'.$this->filename.'.csv',false);
		}else {
			if ($extensionfile==".xlsx") {
				try {
					require(APPPATH.'libraries/XLSXReader.php');
					$xlsx = new XLSXReader('./data/temp_data/archivo_temporal'.$this->filename.'.xlsx');
					$result = $xlsx->getSheetData(2);
				} catch (Exception $e) {
					$result=false;
				}
			}
		}
		if($result){
			$rows =  array();
			$position=0;
			if ($result[0][0]=="RFC") {
				$position=1;
			}
			$Proveedores_System=$this->Proveedor->GetProvedor();
			$estadistic["inserts"]=0;
			$estadistic["inserts_error"]=0;
			$estadistic["update"]=0;
			$estadistic["update_error"]=0;
			$estadistic["total"]=0;

			$lugares["entidades"]=$this->Estados->GetEntidades();
			$lugares["municipios"]=[];
			$lugares["localidades"]=false;

			$estadoselection=array(
				'cveentidad' => 0,
				'cvemunicipio' => 0,
				'cvelocalidad' => 0,
				'lastcveentidad'=>0,
			);

			$contri=array(
				'razon' => "F",
				'tipo' => 1,
			);

			$contribuyente = array(
				'contribuyentes' => $this->Contribuyentes->Getcontri(),
				'tipoproveedores' => $this->Contribuyentes->Gettipoprovee(),
			);

			for ($i=$position; $i < count($result); $i++) {
				if($result[$i][0]!=""){
					if ($lugares["entidades"]) {
						foreach ($lugares["entidades"] as $key => $estado) {
							if (CheckIfStringIsEmpty($result[$i][12])==$estado->desentidad) {
								$estadoselection["cveentidad"]=$estado->cveentidad;
								if ($estadoselection["cveentidad"]=="") {
									$estadoselection["lastcveentidad"]=$estado->cveentidad;
									$lugares["municipios"]=$this->Estados->GetMunicipios($estado->cveentidad);
								}
								if ($estadoselection["lastcveentidad"]==$estado->cveentidad) {
									if ($lugares["municipios"]) {
										foreach ($lugares["municipios"] as $key2 => $municipios) {
											if ($municipios->desmunicipio==CheckIfStringIsEmpty($result[$i][11])) {
												$estadoselection["cvemunicipio"]=$municipios->cvemunicipio;
												if (!$lugares["localidades"]) {
													$lugares["localidades"]=$this->Estados->GetLocalidades($estado->cveentidad,$municipios->cvemunicipio);
												}
												if ($lugares["localidades"]) {
													foreach ($lugares["localidades"] as $key3 => $colonia) {
														if ($colonia->deslocalidad==str_pad(CheckIfStringIsEmpty($result[$i][10]), 100)) {
															$estadoselection["cvelocalidad"]=$colonia->cvelocalidad;
															break;
														}
													}
												}else{
													break;
												}
											}
										}
									}
								}else{
									$lugares["localidades"]=false;
									$estadoselection["lastcveentidad"]=$estado->cveentidad;
									$lugares["municipios"]=$this->Estados->GetMunicipios($estado->cveentidad);
									if ($lugares["municipios"]) {
										foreach ($lugares["municipios"] as $key2 => $municipios) {
											if ($municipios->desmunicipio==CheckIfStringIsEmpty($result[$i][11])) {
												$estadoselection["cvemunicipio"]=$municipios->cvemunicipio;
												if (!$lugares["localidades"]) {
													$lugares["localidades"]=$this->Estados->GetLocalidades($estado->cveentidad,$municipios->cvemunicipio);
												}
												if ($lugares["localidades"]) {
													foreach ($lugares["localidades"] as $key3 => $colonia) {
														if ($colonia->deslocalidad==str_pad(CheckIfStringIsEmpty($result[$i][10]), 100)) {
															$estadoselection["cvelocalidad"]=$colonia->cvelocalidad;
															break;
														}
													}
												}else {
													break;
												}
											}
										}
									}
								}
							}
						}
					}

					if ($contribuyente["contribuyentes"]) {
						foreach ($contribuyente["contribuyentes"] as $key => $value) {
							if ($value->descripcion=="Persona ".CheckIfStringIsEmpty($result[$i][1])) {
								$contri["razon"]=$value->idtipo;
								break;
							}
						}
					}
					if ($contribuyente["tipoproveedores"]) {
						foreach ($contribuyente["tipoproveedores"] as $key => $value) {
							if ($value->descripcion==CheckIfStringIsEmpty($result[$i][2])) {
								$contri["tipo"]=$value->idtipoproveedor;
								break;
							}
						}
					}
					$Proveedor = array(
						'rfc' => mb_strtoupper($result[$i][0]),
						'nombre' => CheckIfStringIsEmpty($result[$i][3]),
						'apellido_p' => CheckIfStringIsEmpty($result[$i][4]),
						'apellido_m' => CheckIfStringIsEmpty($result[$i][5]),
						'nombre_representante' => CheckIfStringIsEmpty($result[$i][6]),
						'calle' => CheckIfStringIsEmpty($result[$i][7]),
						'numero' => CheckIfStringIsEmpty($result[$i][8]),
						'cp' => CheckIfStringIsEmpty($result[$i][9]),
						'cveentidad' => $estadoselection["cveentidad"],
						'cvemunicipio' => $estadoselection["cvemunicipio"],
						'cvelocalidad' => $estadoselection["cvelocalidad"],
						'email' => CheckIfStringIsEmpty($result[$i][13]),
						'telefono' => CheckIfStringIsEmpty($result[$i][14]),
						'fecha_registro' => date("Y-m-d H:i:s"),
						'fecha_actualizacion' => date("Y-m-d H:i:s"),
						'fk_tipo' =>$contri["tipo"],
						'fk_razon' =>$contri["razon"],
						'nacionalidad' =>$result[$i][15],
					);
					if($Proveedores_System){
						$isfind=false;
						foreach ($Proveedores_System as $key => $value) {
							if ($value->rfc==mb_strtoupper($result[$i][0])) {
								$isfind=true;
								if ($this->Proveedor->UpdateProvedor(mb_strtoupper($result[$i][0]),$Proveedor)) {
									$estadistic["update"]+=1;
								}else{
									$estadistic["update_error"]+=1;
								}
								break;
							}
						}
						if(!$isfind){
							if ($this->Proveedor->InsertProvedor($Proveedor)) {
								$estadistic["inserts"]+=1;
							}else{
								$estadistic["inserts_error"]+=1;
							}
						}
					}else{
						if ($this->Proveedor->InsertProvedor($Proveedor)) {
							$estadistic["inserts"]+=1;
						}else{
							$estadistic["inserts_error"]+=1;
						}
					}
					$estadistic["total"]+=1;
				}
			}
		}
		$estadistic["time_elapsed_secs"] = microtime(true) - $estadistic["start"];
		echo json_encode($estadistic);
	}
}
