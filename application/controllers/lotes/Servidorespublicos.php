<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Servidorespublicos extends CI_Controller {
	public $filename="_servidores";
	function __construct() {
		parent::__construct();
		$this->load->helper(
			array(
				'url',
				'form',
				'headers',
				'dates',
				'names',
			)
		);
		$this->load->model(
			array(
				'lotes/Proveedor',
				'sujetos/servidores/Servidores'
			)
		);
	}
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function Cargar($clave='')
	{
		HeaderJson();
		ini_set('post_max_size', '100M');
		ini_set('upload_max_filesize', '100M');
		if (!is_dir("./data/temp_data/")) {
			mkdir("./data/temp_data/", 0777, TRUE);
		}
		$config['upload_path'] = './data/temp_data/';
		$config['allowed_types'] = 'xlsx|csv';
		$config['overwrite'] = TRUE;
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload("archivo"))
		{
			$error = array('error' => $this->upload->display_errors());
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$fa=$this->upload->data();
			rename($fa['full_path'],$fa['file_path'].'archivo_temporal'.$this->filename.$fa["file_ext"]);
			$this->ReadExcel($fa["file_ext"],$clave);
		}
	}
	private	function ReadExcel($extensionfile,$clave="")
	{
		ini_set("max_execution_time", "-1");
		ini_set("memory_limit", "-1");
		ignore_user_abort(true);
		set_time_limit(0);
		$estadistic["start"] = microtime(true);
		$this->load->helper("file");
		$result=false;
		if ($extensionfile==".csv") {
			$this->load->library('csvreader');
			$result = $this->csvreader->parse_file('./data/temp_data/archivo_temporal'.$this->filename.'.csv',false);
		}else {
			if ($extensionfile==".xlsx") {
				try {
					require(APPPATH.'libraries/XLSXReader.php');
					$xlsx = new XLSXReader('./data/temp_data/archivo_temporal'.$this->filename.'.xlsx');
					$result = $xlsx->getSheetData(1);
				} catch (Exception $e) {
					var_dump($e);
					$result=false;
				}
			}
		}
		if($result){
			$rows =  array();
			$position=0;
			if ($result[0][1]=="(2) RFC") {
				$position=1;
			}

			$system_data["profesiones"]=$this->Servidores->GetProfesion();
			$system_data["puestos"]=$this->Servidores->GetPuestos();
			$currentdata["puestos"]=array_column($result,6);
			array_splice($currentdata["puestos"],0,$position);
			$currentdata["puestos"]=array_unique($currentdata["puestos"]);
			$currentdata["puestos"]=array_values(	$currentdata["puestos"]);
			$currentdata["profesiones"]=array_column($result,5);
			array_splice($currentdata["profesiones"],0,$position);
			$currentdata["profesiones"]=array_unique($currentdata["profesiones"]);
			$currentdata["profesiones"]=array_values(	$currentdata["profesiones"]);

			if ($system_data["profesiones"]) {
				$profesionadd=[];
				for ($i=0; $i < count($currentdata["profesiones"]); $i++) {
					$candd=true;
					foreach ($system_data["profesiones"] as $key => $profesiones) {
						if ($currentdata["profesiones"][$i]==$profesiones->nombre) {
							$candd=false;
							break;
						}
					}
					if ($candd) {
						if (!is_null( $currentdata["profesiones"][$i])) {
							$profesionadd[]=array(
								'nombre' => $currentdata["profesiones"][$i],
							);
						}
					}
				}
				if ($profesionadd) {
					$this->Servidores->InsertProfesion($profesionadd);
				}
				$profesionadd=[];
			}else {
				$profesionadd=[];
				$profesionadd[]=array(
					'nombre' =>"",
				);
				for ($i=0; $i < count($currentdata["profesiones"]); $i++) {
					$profesionadd[]=array(
						'nombre' => $currentdata["profesiones"][$i],
					);
				}
				$this->Servidores->InsertProfesion($profesionadd);
				$profesionadd=[];
			}

			if ($system_data["puestos"]) {
				$puestoadd=[];
				for ($i=0; $i < count($currentdata["puestos"]); $i++) {
					$candd=true;
					foreach ($system_data["puestos"] as $key => $puestos) {
						if ($currentdata["puestos"][$i]==$puestos->nombre) {
							$candd=false;
							break;
						}
					}
					if ($candd) {
						$puestoadd[]=array(
							'nombre' => $currentdata["puestos"][$i],
							'descripcion' =>"",
						);
					}
				}
				if ($puestoadd) {
					$this->Servidores->InsertPuestos($puestoadd);
				}
				$puestoadd=[];
			}else {
				$puestoadd=[];
				$puestoadd[]=array(
					'nombre' =>"",
				);
				for ($i=0; $i < count($currentdata["puestos"]); $i++) {
					$puestoadd[]=array(
						'nombre' => $currentdata["puestos"][$i],
						'descripcion' =>"",
					);
				}
				if ($puestoadd) {
					$this->Servidores->InsertPuestos($puestoadd);
				}
				$puestoadd=[];
			}


			$currentdata["profesiones"]=[];
			$currentdata["puestos"]=[];

			$system_data["profesiones"]=$this->Servidores->GetProfesion();
			$system_data["puestos"]=$this->Servidores->GetPuestos();
			$system_data["servidores"]=$this->Servidores->GetServidoresPublicosEnte($clave);
			$system_data["proveedoressujeto"]=$this->Servidores->GetProveedoresSujeto($clave);
			$system_data["proveedores"]=$this->Servidores->GetProveedores();

			$estadistic["inserts"]=0;
			$estadistic["inserts_error"]=0;
			$estadistic["update"]=0;
			$estadistic["update_error"]=0;
			$estadistic["total"]=0;

			$Servidor["insert"]=[];
			$Servidor["update"]=[];
			$Servidor["cambios"]=[];
			$Servidor["proveedorrelacion"]=[];
			for ($i=$position; $i < count($result); $i++) {
				if($result[$i][0]=="Servidor"){
					$canInsert=true;
					$id["profesion"]=0;
					$id["puestos"]=0;
					foreach ($system_data["profesiones"] as $key => $pro) {
						if ($pro->nombre==$result[$i][5]) {
							$id["profesion"]=$pro->idcat_profesion;
							break;
						}
					}
					foreach ($system_data["puestos"] as $key => $pues) {
						if ($pues->nombre==$result[$i][6]) {
							$id["puestos"]=$pues->idcat_puestos;
							break;
						}
					}
					if ($system_data["servidores"]) {
						foreach ($system_data["servidores"] as $key => $value) {
							if ($value->rfc==mb_strtoupper($result[$i][1])) {
								$canInsert=false;
								if ($result[$i][3]=="" && $result[$i][4]=="") {
									$nombresretornados=SplitName(rtrim($result[$i][2]));
								}else{
									$nombresretornados["Paterno"]=rtrim($result[$i][3]);
									$nombresretornados["Materno"]= rtrim($result[$i][4]);
									$nombresretornados["Nombres"]=rtrim($result[$i][2]);
								}
								$Servidor["update"][] = array(
									'rfc' => mb_strtoupper($result[$i][1]),
									'nombre' => $nombresretornados["Nombres"] ,
									'apellido_pa' => $nombresretornados["Paterno"],
									'apellido_ma' => 	$nombresretornados["Materno"],
									'tel' => strval($result[$i][7]),
									'celular' => strval($result[$i][8]),
									'ine' => $result[$i][9],
									'calle' => $result[$i][10],
									'cp' => strval($result[$i][11]),
									'numint' => strval($result[$i][12]),
									'numext' => strval($result[$i][13]),
									'fini' =>GetExceldDate($result[$i][14]),
									'ffin' => GetExceldDate($result[$i][15]),
									'cat_profesion_idcat_profesion' => $id["profesion"],
									'cat_puestos_idcat_puestos' => $id["puestos"],
									'fk_clave'=>$clave,
								);
								$Servidor["cambios"][] = array(
									'fk_rfc' => $value->rfc,
									'nombre' => $value->nombre,
									'apellido_pa' => $value->apellido_pa,
									'apellido_ma' => 	$value->apellido_ma,
									'tel' => $value->tel,
									'celular' => $value->celular,
									'ine' => $value->ine,
									'calle' => $value->calle,
									'cp' => $value->cp,
									'numint' => $value->numint,
									'numext' => $value->numext,
									'fini' =>$value->fini,
									'ffin' => $value->ffin,
									'cat_profesion_idcat_profesion' => $value->cat_profesion_idcat_profesion,
									'cat_puestos_idcat_puestos' => $value->cat_puestos_idcat_puestos,
									'idusuario'=>1,
									'fechacambio'=>date("Y-m-d H:s:i"),
								);
								break;
							}
						}
						if ($canInsert) {
							if($result[$i][0]!=""){
								if ($result[$i][3]=="" && $result[$i][4]=="") {
									$nombresretornados=SplitName(rtrim($result[$i][2]));
								}else{
									$nombresretornados["Paterno"]=rtrim($result[$i][3]);
									$nombresretornados["Materno"]= rtrim($result[$i][4]);
									$nombresretornados["Nombres"]=rtrim($result[$i][2]);
								}
								$Servidor["insert"][] = array(
									'rfc' => mb_strtoupper($result[$i][1]),
									'nombre' => $nombresretornados["Nombres"] ,
									'apellido_pa' => $nombresretornados["Paterno"],
									'apellido_ma' => 	$nombresretornados["Materno"],
									'tel' => strval($result[$i][7]),
									'celular' => strval($result[$i][8]),
									'ine' => $result[$i][9],
									'calle' => $result[$i][10],
									'cp' => strval($result[$i][11]),
									'numint' => strval($result[$i][12]),
									'numext' => strval($result[$i][13]),
									'fini' =>GetExceldDate($result[$i][14]),
									'ffin' => GetExceldDate($result[$i][15]),
									'cat_profesion_idcat_profesion' => $id["profesion"],
									'cat_puestos_idcat_puestos' => $id["puestos"],
									'fk_clave'=>$clave,
								);
							}
						}
					}else{
						if($result[$i][0]!=""){
							if ($result[$i][3]=="" && $result[$i][4]=="") {
								$nombresretornados=SplitName(rtrim($result[$i][2]));
							}else{
								$nombresretornados["Paterno"]=rtrim($result[$i][3]);
								$nombresretornados["Materno"]= rtrim($result[$i][4]);
								$nombresretornados["Nombres"]=rtrim($result[$i][2]);
							}
							$Servidor["insert"][] = array(
								'rfc' => mb_strtoupper($result[$i][1]),
								'nombre' => $nombresretornados["Nombres"] ,
								'apellido_pa' => $nombresretornados["Paterno"],
								'apellido_ma' => 	$nombresretornados["Materno"],
								'tel' => strval($result[$i][7]),
								'celular' => strval($result[$i][8]),
								'ine' => $result[$i][9],
								'calle' => $result[$i][10],
								'cp' => strval($result[$i][11]),
								'numint' => strval($result[$i][12]),
								'numext' => strval($result[$i][13]),
								'fini' =>GetExceldDate($result[$i][14]),
								'ffin' => GetExceldDate($result[$i][15]),
								'cat_profesion_idcat_profesion' => $id["profesion"],
								'cat_puestos_idcat_puestos' => $id["puestos"],
								'fk_clave'=>$clave,
							);
						}
					}
					$estadistic["total"]+=1;
				}
				if ($result[$i][0]=="Proveedor") {
					if ($system_data["proveedoressujeto"]) {
						$canRela=true;
						foreach ($system_data["proveedoressujeto"] as $key => $value) {
							if ($value->fk_rfc==mb_strtoupper($result[$i][1])) {
								$canRela=false;
								break;
							}
						}
						if ($canRela) {
							if ($system_data["proveedores"]) {
								$candRelaprov=false;
								foreach ($system_data["proveedores"] as $key => $value) {
									if ($value->rfc==mb_strtoupper($result[$i][1])) {
										$candRelaprov=true;
										break;
									}
								}
								if ($candRelaprov) {
									$Servidor["proveedorrelacion"][]= array(
										'fk_sujeto' => $clave,
										'fk_rfc' =>$result[$i][1],
									);
								}else{
									// se agregaria si no existe
								}
							}else{
								$Servidor["proveedorrelacion"][]= array(
									'fk_clave' => $clave,
									'fk_rfc' =>$result[$i][1],
								);
							}

						}
					}else{
						if ($system_data["proveedores"]) {
							$candRelaprov=false;
							foreach ($system_data["proveedores"] as $key => $value) {
								if ($value->rfc==mb_strtoupper($result[$i][1])) {
									$candRelaprov=true;
									break;
								}
							}
							if ($candRelaprov) {
								$Servidor["proveedorrelacion"][]= array(
									'fk_clave' => $clave,
									'fk_rfc' =>$result[$i][1],
								);
							}else{

							}
						}else{
							/*$Servidor["proveedorrelacion"][]= array(
								'fk_sujeto' => $clave,
								'fk_rfc' =>$result[$i][1],
							);*/
						}
					}
				}
			}
			if ($Servidor["insert"]) {
				if ($this->Servidores->InsertServidor($Servidor["insert"])) {

				}
			}
			if ($Servidor["cambios"]) {
				$this->Servidores->InsertServidorCambios($Servidor["cambios"]);
			}
			if ($Servidor["update"]) {
				$this->Servidores->UpdateServidor($Servidor["update"]);
			}
			if ($Servidor["proveedorrelacion"]) {
				if ($this->Servidores->InsertProveedorSuejto($Servidor["proveedorrelacion"])) {

				}
			}
		}
		$estadistic["time_elapsed_secs"] = microtime(true) - $estadistic["start"];
		echo json_encode($estadistic);
	}
}
