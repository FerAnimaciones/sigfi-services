<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sujetosfiscalizables extends CI_Controller {
  public $filename="_sujetos";
  function __construct() {
    parent::__construct();
    $this->load->helper(
      array(
        'url',
        'form',
        'headers',
        'lotes',
      )
    );
    $this->load->model(
      array(
        'lotes/Proveedor',
        'Estados',
        'sujetos/fiscalizables/Fiscalizables',
      )
    );
  }
  public function index()
  {
    $this->load->view('welcome_message');
  }
  public function Cargar($value='')
  {
    HeaderJson();
    ini_set('post_max_size', '100M');
    ini_set('upload_max_filesize', '100M');
    if (!is_dir("./data/temp_data/")) {
      mkdir("./data/temp_data/", 0777, TRUE);
    }
    $config['upload_path'] = './data/temp_data/';
    $config['allowed_types'] = 'xlsx|csv';
    $config['overwrite'] = TRUE;
    $this->load->library('upload', $config);
    if (!$this->upload->do_upload("archivo"))
    {
      $error = array('error' => $this->upload->display_errors());
    }
    else
    {
      $data = array('upload_data' => $this->upload->data());
      $fa=$this->upload->data();
      rename($fa['full_path'],$fa['file_path'].'archivo_temporal'.$this->filename.$fa["file_ext"]);
      $this->ReadExcel($fa["file_ext"]);
    }
  }
  private	function ReadExcel($extensionfile)
  {
    ini_set("max_execution_time", "-1");
    ini_set("memory_limit", "-1");
    ignore_user_abort(true);
    set_time_limit(0);
    $estadistic["start"] = microtime(true);
    $this->load->helper("file");
    $result=false;
    if ($extensionfile==".csv") {
      $this->load->library('csvreader');
      $result = $this->csvreader->parse_file('./data/temp_data/archivo_temporal'.$this->filename.'.csv',false);
    }else {
      if ($extensionfile==".xlsx") {
        try {
          require(APPPATH.'libraries/XLSXReader.php');
          $xlsx = new XLSXReader('./data/temp_data/archivo_temporal'.$this->filename.'.xlsx');
          $dat=$xlsx->getSheetNames();
          $result = $xlsx->getSheetData(2);
        } catch (Exception $e) {
          $result=false;
        }
      }
    }
    if($result){
      $rows =  array();
      $position=0;
      if ($result[0][0]=="Clave") {
        $position=1;
      }
      $Sujetos_System=$this->Fiscalizables->GetSujetosFiscalizables();
      $TipoEntes=$this->Fiscalizables->GetCatTipoEntes();
      $TipoEntesClase=$this->Fiscalizables->GetClasesTipoEnte();

      $estadistic["inserts"]=0;
      $estadistic["inserts_error"]=0;
      $estadistic["update"]=0;
      $estadistic["update_error"]=0;
      $estadistic["total"]=0;

      $lugares["entidades"]=$this->Estados->GetEntidades();
      $lugares["municipios"]=[];
      $lugares["localidades"]=false;

      $estadoselection=array(
        'cveentidad' => 0,
        'cvemunicipio' => 0,
        'cvelocalidad' => 0,
        'lastcveentidad'=>0,
      );
      $contri["cat_tipo_entes_idcat_tipo_entes"]=0;

      $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
      'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
      'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
      'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
      'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );

      $insert["sujetos"]=[];
      $update["sujetos"]=[];

      for ($i=$position; $i < count($result)-1; $i++) {
        if ($TipoEntes) {
          foreach ($TipoEntes as $key => $value) {
            if ($value->nombre!=remove_accents(CheckIfStringIsEmpty($result[$i][2]))) {

            }
          }
        }
      }

      for ($i=$position; $i < count($result)-1; $i++) {
        $estadoselection=array(
          'cveentidad' => 0,
          'cvemunicipio' => 0,
          'cvelocalidad' => 0,
          'lastcveentidad'=>0,
        );
        $contri["cat_tipo_entes_idcat_tipo_entes"]=1;
        $contri["clase"]=1;
        $contri["clase_t"]="";
        if($result[$i][0]!=""){
          if ($TipoEntes) {
            foreach ($TipoEntes as $key => $value) {
              if ($value->nombre==CheckIfStringIsEmpty($result[$i][2])) {
                $contri["cat_tipo_entes_idcat_tipo_entes"]=$value->idcat_tipo_entes;
                if ($TipoEntesClase) {
                  foreach ($TipoEntesClase as $key => $value2) {
                    if ($value->idcat_tipo_entes==$value2->fk_tipo) {
                      if ($value2->descripcion==$result[$i][3]) {
                        $contri["clase"]=$value2->fkclase;
                        $contri["clase_t"]=$value2->tipo;
                      }else{
                        $contri["clase"]=$value2->fkclase;
                        $contri["clase_t"]=$value2->tipo;
                      }
                    }
                  }
                }
                break;
              }
              if (remove_accents($value->nombre)==CheckIfStringIsEmpty(remove_accents($result[$i][2]))) {
                $contri["cat_tipo_entes_idcat_tipo_entes"]=$value->idcat_tipo_entes;
                if ($TipoEntesClase) {
                  foreach ($TipoEntesClase as $key => $value2) {
                    if ($value->idcat_tipo_entes==$value2->fk_tipo) {
                      if ($value2->descripcion==$result[$i][3]) {
                        $contri["clase"]=$value2->fkclase;
                        $contri["clase_t"]=$value2->tipo;
                      }else{
                        $contri["clase"]=$value2->fkclase;
                        $contri["clase_t"]=$value2->tipo;
                      }
                    }
                  }
                }
                break;
              }
            }
          }

          if ($lugares["entidades"]) {
            foreach ($lugares["entidades"] as $key => $estado) {
              if (CheckIfStringIsEmpty(strtr(mb_strtoupper($result[$i][11]),$unwanted_array))==$estado->desentidad) {
                $estadoselection["cveentidad"]=$estado->cveentidad;
                if ($estadoselection["cveentidad"]=="") {
                  $estadoselection["lastcveentidad"]=$estado->cveentidad;
                  $lugares["municipios"]=$this->Estados->GetMunicipios($estado->cveentidad);
                }
                if ($estadoselection["lastcveentidad"]==$estado->cveentidad) {
                  if ($lugares["municipios"]) {
                    foreach ($lugares["municipios"] as $key2 => $municipios) {
                      if ($municipios->desmunicipio==CheckIfStringIsEmpty(strtr(mb_strtoupper($result[$i][10]),$unwanted_array))) {
                        $estadoselection["cvemunicipio"]=$municipios->cvemunicipio;
                        if (!$lugares["localidades"]) {
                          $lugares["localidades"]=$this->Estados->GetLocalidades($estado->cveentidad,$municipios->cvemunicipio);
                        }
                        if ($lugares["localidades"]) {
                          foreach ($lugares["localidades"] as $key3 => $colonia) {
                            if ($colonia->deslocalidad==str_pad(CheckIfStringIsEmpty(strtr(mb_strtoupper($result[$i][9]),$unwanted_array)), 100)) {
                              $estadoselection["cvelocalidad"]=$colonia->cvelocalidad;
                              break;
                            }
                          }
                        }else{
                          break;
                        }
                      }
                    }
                  }
                }else{
                  $lugares["localidades"]=false;
                  $estadoselection["lastcveentidad"]=$estado->cveentidad;
                  $lugares["municipios"]=$this->Estados->GetMunicipios($estado->cveentidad);
                  if ($lugares["municipios"]) {
                    foreach ($lugares["municipios"] as $key2 => $municipios) {
                      if ($municipios->desmunicipio==CheckIfStringIsEmpty(strtr(mb_strtoupper($result[$i][10]),$unwanted_array))) {
                        $estadoselection["cvemunicipio"]=$municipios->cvemunicipio;
                        if (!$lugares["localidades"]) {
                          $lugares["localidades"]=$this->Estados->GetLocalidades($estado->cveentidad,$municipios->cvemunicipio);
                        }
                        if ($lugares["localidades"]) {
                          foreach ($lugares["localidades"] as $key3 => $colonia) {
                            if ($colonia->deslocalidad==str_pad(CheckIfStringIsEmpty(strtr(mb_strtoupper($result[$i][9]),$unwanted_array)), 100)) {
                              $estadoselection["cvelocalidad"]=$colonia->cvelocalidad;
                              break;
                            }
                          }
                        }else {
                          break;
                        }
                      }
                    }
                  }
                }
              }
            }
          }

          $Sujetos = array(
            'clave' => mb_strtoupper($result[$i][0]),
            'nombre_ente' => $result[$i][1],
            'tipo' => "",
            'calle' => $result[$i][4],
            'numint' => strval( $result[$i][5]),
            'numext' => strval( $result[$i][6]),
            'colonia' => $result[$i][7],
            'cp' => strval( $result[$i][8]),
            'estado' => $estadoselection["cveentidad"],
            'municipio' =>$estadoselection["cvemunicipio"],
            'localidad' => $estadoselection["cvelocalidad"],
            'cat_tipo_entes_idcat_tipo_entes' =>$contri["cat_tipo_entes_idcat_tipo_entes"],
            'fk_clase' =>  $contri["clase"],
            'tipoclase'=>$contri["clase_t"],
          );
          if($Sujetos_System){
            $isfind=false;
            foreach ($Sujetos_System as $key => $value) {
              if ($value->clave==mb_strtoupper($result[$i][0])) {
                $isfind=true;
                $update["sujetos"][]=$Sujetos;
                break;
              }
            }
            if(!$isfind){
              $insert["sujetos"][]=$Sujetos;

            }
          }else{
            $insert["sujetos"][]=$Sujetos;
          }
          $estadistic["total"]+=1;
        }
      }

      if ($insert["sujetos"]) {
        if ($this->Fiscalizables->InsertSujetoFiscalizable(  $insert["sujetos"])) {
          $estadistic["inserts"]+=1;
        }else{
          $estadistic["inserts_error"]+=1;
        }
      }
      if ($update["sujetos"]) {
        if ($this->Fiscalizables->UpdateSujetoFiscalizable($update["sujetos"])) {
          $estadistic["update"]+=1;
        }else{
          $estadistic["update_error"]+=1;
        }
      }
    }
    $estadistic["time_elapsed_secs"] = microtime(true) - $estadistic["start"];
    echo json_encode($estadistic);
  }
}
