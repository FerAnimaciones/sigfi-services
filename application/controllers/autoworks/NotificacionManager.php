<?php defined('BASEPATH') OR exit('No direct script access allowed');
class NotificacionManager extends CI_Controller {
  public $correosenviados=0;
  public $limitecorreos=5;
  // php index.php autoworks notificacionmanager notifications
  // */15 * * * * /usr/bin/php /var/www/html/api/sigfi-services/index.php autoworks NotificacionManager notifications
  // sudo systemctl restart crond.service
  //sudo cat /etc/crontab


  function __construct() {
    parent::__construct();
    $this->load->helper(array('url', 'form','headers','jwt','auth','money'));
    $this->load->model(array('planeacion/Programa','Correos'));
    $this->lang->load('email', 'spanish');

  }
  public function index()
  {
    $this->load->view('welcome_message');
  }
  public function Notifications()
  {
    if (true) {
      $this->ProgramasAuditoria();
      echo "Correos enviados: $this->correosenviados";
    }
  }
  private function ProgramasAuditoria($value='')
  {
    if (true) {
      $data["correosenvio"] = array('carobox96@gmail.com','feranimaciones@gmail.com','jlsh23@hotmail.com','xvaliente2311@gmail.com','lalyssalas@hotmail.com');
      $data["programaauditoria"]=$this->Programa->GetTodayProgramaAuditoria();
      if ($data["programaauditoria"]) {
        foreach ($data["programaauditoria"] as $key => $value) {
          if ($this->correosenviados<$this->limitecorreos) {
            $data["programastipo"]=$this->Programa->GetTodayProgramaTipoDateProgram($value->idprogramaauditoria);
            if ($data["programastipo"]) {
              $data["nextnotification"]=date("Y-m-d H:i:s", time() + 86400);
              $data["programamaactual"]=$value;
              $bodymail=$this->load->view('email/format/programaauditoria.php',$data,TRUE);
              $datoscorreo["titulo"]=$this->lang->line('subjet_programa_fechas');
              $datoscorreo["body"]=$bodymail;
              try {
                $reponse=$this->Correos->EnviarCorreo($datoscorreo,$data["correosenvio"]);
                if ($reponse["send"]) {
                  if ($data["programastipo"]) {
                    $update;
                    foreach ($data["programastipo"] as $key => $value) {
                      $update[] =
                      array(
                        'idprogramaauditoriatipo' => $value->idprogramaauditoriatipo ,
                        'fhnotificacionesfechas' => $data["nextnotification"]
                      );
                    }
                    $this->Programa->GetUpdatePgAT($update);
                  }
                  $this->correosenviados++;
                }else{

                }
              } catch (\Exception $e) {

              }
            }
          }
        }
      }
    }
  }
  public function Preview($value='')
  {  // http://localhost:8080/api/sigfi-services/index.php/autoworks/NotificacionManager/preview
    $data=false;
    $data["base_system"]="http://201.107.4.145/";
    $data["systeminfo"]=$this->Systeminfo->GetInfoSystema();
    $datoscorreo["titulo"]=$this->lang->line('subjet_programa_fechas');
    $data["body"]=$this->load->view('email/format/programaauditoria.php',$data,TRUE);
    $emailbody = $this->load->view('email/email.php',$data,TRUE);
    echo $emailbody;
  }

}
