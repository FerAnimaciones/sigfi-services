<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;

class Oficios extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper(array('url', 'form','headers','jwt','auth','money','dates','headersoffices'));
		$this->load->model(array('planeacion/Planeacion','Direcciones'));
	}
	public function index()
	{
		HeaderJson();
		$arrayName["system"]="sigfi";
		echo json_encode($arrayName);
	}
	/**
	*
	*
	* @param null
	*
	* @return void
	*/
	public function Oficioauditoria($id="",$id2="")
	{
		HeaderJson();
		if ($this->input->method()=="get" ) {
			try {
				$data["programauditoria"]=$this->Planeacion->GetProgramaAuditoria($id,$id2);
				$data["depas"]=$this->Direcciones->Getdirbyid(	$data["programauditoria"][0]->fk_diradmin);
				$options = [
					'alwaysShowDecimals' => true,
					'nbDecimals' => 2,
					'decPoint' => ".",
					'thousandSep' => "",
					'moneySymbol' => "$",
					'moneyFormat' => "sv", // v represents the value, s represents the money symbol
				];
				$nombrebente="";
				$claveoficio="";

				$templateProcessor;
				foreach ($data["programauditoria"] as $key => $item){
					if($item->cat_tipo_auditoria_idcat_tipo_auditoria==2) {
						$templateProcessor = new TemplateProcessor('formatos/planeacion/Programaauditoriafinan.docx');
					}
					if($item->cat_tipo_auditoria_idcat_tipo_auditoria==3) {
						$templateProcessor = new TemplateProcessor('formatos/planeacion/Programaauditoriapublic.docx');
					}
					if($item->cat_tipo_auditoria_idcat_tipo_auditoria==4) {
						$templateProcessor = new TemplateProcessor('formatos/planeacion/Programaauditoriadesen.docx');
					}
				}

				//$templateProcessor = new TemplateProcessor('formatos/planeacion/Programaauditoria.docx');
				$templateProcessor->setImageValue('ente', 'asen.png');
				if ($data["programauditoria"]) {
					$data["personal"]=$this->Planeacion->GetProgramaAuditoriaTipoPersonal($id2);
					if ($data["personal"]) {
						$templateProcessor->cloneRow('nombre',count($data["personal"]));
						$temp1=1;
						foreach ($data["personal"] as $key => $item){
							$templateProcessor->setValue('nombre#'.$temp1, $item->nombre." ".$item->apellido_pa." ".$item->apellido_ma);
							$templateProcessor->setValue('puesto#'.$temp1, $item->cargo);
							$temp1++;
						}
					}

					foreach ($data["programauditoria"] as $key => $item){
						$nombrebente=$item->nombre_ente;
						$claveoficio=$item->numeroauditoria;

						$templateProcessor->setValue('aniofiscal',  $item->aafiscal);
						$templateProcessor->setValue('sujeto', $item->nombre_ente);
						$templateProcessor->setValue('numeroauditoria', $item->numeroauditoria);
						$templateProcessor->setValue('descripcionauditoria', $item->auditoria);
						$templateProcessor->setValue('descripcionmodalidad', $item->modalidadejecdes);
						$templateProcessor->setValue('nombreareaaudi',$data["depas"][0]->nombre);
						$templateProcessor->setValue('descripcioncriterio', $item->cfdes);
						$templateProcessor->setValue('objetivo', $item->objetivo);
						$templateProcessor->setValue('estado', $item->desentidad);

						$templateProcessor->setValue('muestra', formatPrice($item->muestra,$options));
						$templateProcessor->setValue('alcance', $item->alcance);
						$templateProcessor->setValue('universo', formatPrice($item->universo, $options));
						$templateProcessor->setValue('muestrax', $item->muestrax);
						$templateProcessor->setValue('alcancex', $item->alcancex);
						$templateProcessor->setValue('universox', $item->universox);

						if($item->fechas=='t'){
							$templateProcessor->setValue('fechaini',  date("d/m/Y", strtotime($item->fhapertura)));
							$templateProcessor->setValue('fechafin',  date("d/m/Y", strtotime($item->fhcierre)));
							$templateProcessor->setValue('duracion', $item->duracionauditoria);
							$templateProcessor->setValue('fechapresentacion', date("d/m/Y", strtotime($item->fhpresentacion)));
						}else{
							$templateProcessor->setValue('fechaini',  'SIN CAPTURAR');
							$templateProcessor->setValue('fechafin', 'SIN CAPTURAR' );
							$templateProcessor->setValue('duracion', 'SIN CAPTURAR');
							$templateProcessor->setValue('fechapresentacion', 'SIN CAPTURAR');
						}
					}
					HeaderWORD("Programa Auditoria ");
					$templateProcessor->saveAs("php://output");
				}else{
					$data["message"]="Ocurrio un error.";
					echo json_encode($data["message"]);
				}
			} catch (Exception $e) {
				$data["message"]=$e->getMessage();
				echo json_encode($data["message"]);
			}
		}else{
			$data["message"]="Ocurrio un error.";
			echo json_encode($data["message"]);
		}
	}
	/**
	*
	*
	* @param null
	*
	* @return void
	*/
	function Ordenauditoria($idorden='') {
		HeaderJson();
		if ($this->input->method()=="post" ) {
			$datosdelpost = json_decode(file_get_contents('php://input'));
			//	var_dump($datosdelpost);
			try {
				$templateProcessor = new TemplateProcessor('formatos/ejecucion/auditoria/inicio/Orden_DO_AF_DP_OB.docx');

				$templateProcessor->setValue('numeroorden', $datosdelpost->numeroauditoria);
				$templateProcessor->setValue('numeroauditoria', $datosdelpost->numeroauditoria);
				$templateProcessor->setValue('ente', 	$datosdelpost->sujeto->nombre_ente);
				$templateProcessor->setValue('ente_domicilio',$datosdelpost->sujeto->calle);

				$templateProcessor->setValue('aniocp',  date("Y", strtotime('2019')));
				$templateProcessor->setValue('ano',date("Y"));
				$templateProcessor->setValue('diaactual', GetMonth(strtotime(date("Y-m-d H:s:i"))));
				$templateProcessor->setValue('diashabiles',80);


				$objetivos="Ovjetivo 1 objetivo 2 objetivo 3";

				$templateProcessor->setValue('objetivos',$objetivos);

				$templateProcessor->setValue('documentacion',$objetivos);

				$data["personal"]=[];
				foreach ($datosdelpost->tipoaudidata as $key => $value) {
					if (isset($value->personalauditor)) {
						foreach ($value->personalauditor as $key => $personal) {
							//var_dump($personal);
							$temp =
							array(
								'nombre' => $personal->nombre,
								'apellido_pa' =>  $personal->apellido_pa,
								'apellido_ma' =>  $personal->apellido_ma,
								'cargo' => $personal->nombre_cargo,
							);
							array_push($data["personal"],$temp);
						}
					}
				}

				if ($data["personal"]) {
					$templateProcessor->cloneRow('nombre',count($data["personal"]));
					$temp1=1;
					foreach ($data["personal"] as $key => $item){
						$templateProcessor->setValue('nombre#'.$temp1, $item["nombre"]." ".$item["apellido_pa"]." ".$item["apellido_ma"]);
						$templateProcessor->setValue('puesto#'.$temp1, $item["cargo"]);
						$temp1++;
					}
				}
				HeaderWORD("Orden Auditoria");
				$templateProcessor->saveAs("php://output");
			} catch (Exception $e) {
				$data["message"]=$e->getMessage();
				echo json_encode($data["message"]);
			}
		}
	}
	/**
	*
	*
	* @param null
	*
	* @return void
	*/
	function GenerarOficiosComosion($ipos=0,$programa=0) {
		HeaderJson();
		if ($this->input->method()=="post" ) {
			$datosdelpost = json_decode(file_get_contents('php://input'));
			$numof=0;
			if ($datosdelpost->detalleorden[$ipos]->numerocomision <10) {
				$numof="0".$datosdelpost->detalleorden[$ipos]->numerocomision;
			}else{
				$numof=$datosdelpost->detalleorden[$ipos]->numerocomision;
			}
			$tipoaudiclave=$datosdelpost->programaauditoria->tipoaudidata[$ipos]->cat_tipo_auditoria_clave;

			$aniocp=$datosdelpost->programaauditoria->aafiscal;
			$ente=$datosdelpost->programaauditoria->sujeto->nombre_ente;
			$clave=$datosdelpost->programaauditoria->sujeto->clave;
			$claveprograma=$datosdelpost->programaauditoria->numeroauditoria;

			$director=$this->Planeacion->GetDirectorUnidadTipoAuditoria($datosdelpost->programaauditoria->tipoaudidata[$ipos]->cat_tipo_auditoria_idcat_tipo_auditoria);

			$nombredirectoraudi="";
			$puesto="";
			$tipoaudi=$datosdelpost->programaauditoria->tipoaudidata[$ipos]->cat_tipo_auditoria_nombre;
			if ($director) {
				foreach ($director as $key => $value) {
					$nombredirectoraudi=$value->nombre." ".$value->apellido_pa." ".$value->apellido_ma;
					$puesto=$value->puesto;
				}
			}

			try {
				$templateProcessor = new TemplateProcessor('formatos/ejecucion/auditoria/inicio/oficomision.docx');
				$templateProcessor->setValue('oficio', '/'.$tipoaudiclave.'/'.$clave.'/OC-'.$numof.'-'.$aniocp);
				$templateProcessor->setValue('claveprograma', $claveprograma);
				$templateProcessor->setValue('ente', $ente);
				$templateProcessor->setValue('director', $nombredirectoraudi);
				$templateProcessor->setValue('puesto', $puesto.' de '.$tipoaudi);

				$templateProcessor->setValue('aniocp',$aniocp);
				$templateProcessor->setValue('ano',date("Y"));
				$templateProcessor->setValue('diaactual', GetMonth(strtotime(date("Y-m-d H:s:i"))));

				HeaderWORD("Orden Auditoria");
				$templateProcessor->saveAs("php://output");
			} catch (Exception $e) {
				$data["message"]=$e->getMessage();
				echo json_encode($data["message"]);
			}
		}
	}
	public function Comisionauditoriadom()
	{

		try {
			$templateProcessor = new TemplateProcessor('formatos/ejecucion/auditoria/inicio/oficomision.docx');
			$templateProcessor->setValue('oficio','ASEN/AD/EA.03/OC-01/2018');
			$templateProcessor->setValue('fecha','25 de abril de 2018');
			$templateProcessor->setValue('ente','Cecilia del Carmen Lomelí Villarreal ' );
			$templateProcessor->setValue('numaudi','17-EA.03-AF-AD ');
			$templateProcessor->setValue('puesto','Directora de Auditoría de Evaluación al Desempeño');

			$templateProcessor->setValue('lugar','Tribunal de Justicia Administrativa');
			$templateProcessor->setValue('ano','2017');
			$templateProcessor->setValue('director',"LIC. HÉCTOR MANUEL BENÍTEZ PINEDA");
			$templateProcessor->setValue('dirpue','ENCARGADO POR MINISTERIO DE LEY DEL DESPACHO DE LA AUDITORÍA SUPERIOR DEL ESTADO DE NAYARIT');

			HeaderWORD("Orden Auditoria");
			$templateProcessor->saveAs("php://output");
		} catch (Exception $e) {
			$data["message"]=$e->getMessage();
			echo json_encode($data["message"]);
		}
	}

	/**
	*
	*
	* @param null
	*
	* @return void
	*/
	public function GenerateXMLPHP($content='')
	{
		$phpWord = new PhpWord();
		$section = $phpWord->addSection();

		$doc = new DOMDocument();
		$doc->loadHTML($content);
		$doc->saveHTML();
		\PhpOffice\PhpWord\Shared\Html::addHtml($section, $doc->saveHtml(),true);

		$xmlWriter = new XMLWriter(XMLWriter::STORAGE_MEMORY, './', WordSettings::hasCompatibility());
		$containerWriter = new Container($xmlWriter, $section);
		return $containerWriter->write();
	}
}
