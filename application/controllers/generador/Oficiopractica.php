<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;

class Oficiopractica extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper(array('url', 'form','headers','jwt','auth','money','dates','headersoffices'));
		$this->load->model(array('planeacion/Planeacion','Direcciones'));
	}
	public function index()
	{
		HeaderJson();
		$arrayName["system"]="sigfi";
		echo json_encode($arrayName);
	}
	/**
	*
	*
	* @param null
	*	@url http://localhost:8080/api/sigfi-services/index.php/generador/oficiopractica/oficiosiat
	* @return void
	*/
	public function oficiosiat($id="",$id2="")
	{
		HeaderJson();
		if ($this->input->method()=="get" ) {
			try {
				$options = [
					'alwaysShowDecimals' => true,
					'nbDecimals' => 2,
					'decPoint' => ".",
					'thousandSep' => "",
					'moneySymbol' => "$",
					'moneyFormat' => "sv", // v represents the value, s represents the money symbol
				];
				$templateProcessor;
				$templateProcessor = new TemplateProcessor('formatos/ejecucion/auditoria/practica/Oficio_Compulsa_Terceros.docx');
				$templateProcessor->setValue('fecha',GetMonth(strtotime(date("Y-m-d H:s:i"))));
				$replacements = array(
					array('desc' => 'Batman'),
					array('desc' => 'Superman'),
				);
				$templateProcessor->cloneBlock('lista', 0, true, false, $replacements);

				HeaderWORD("Documento");
				$templateProcessor->saveAs("php://output");
			}
			catch (Exception $e) {
				$data["message"]=$e->getMessage();
				echo json_encode($data["message"]);
			}
		}else{
			$data["message"]="Ocurrio un error.";
			echo json_encode($data["message"]);
		}
	}
}
