<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Reportefinal extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper(array('url', 'form','headers','jwt','auth','money','dates','headersoffices'));
		$this->load->model(array(
			'planeacion/Planeacion',
			'planeacion/Programa'
		));
	}
	public function index()
	{
		HeaderJson();
		$arrayName["system"]="sigfi";
		echo json_encode($arrayName);
	}
	public function Reporte($aafiscal)
	{
		$datos["tipos_auditoria"]=$this->Programa->GetTipoAuditorias();
		$datos["pagina1"]=$this->Programa->GetConsultaTipoNumAudi($aafiscal);
		$datos["pagina2"]["tipoente"]=$this->Programa->GetSujetosByTipoEnte();
		$datos["pagina2"]["suejto_entes"]=$this->Programa->GetSujetosByTipoEnteAll($aafiscal);

		$datos["pagina2"]["poderes_auditoria"]=$this->Programa->GetAuditoriasTipoEnte($aafiscal,"es");
		$datos["pagina2"]["otros_auditoria"]=$this->Programa->GetAuditoriasTipoEnte($aafiscal,"ay");

		$colors["gray_black"]='818587';
		$colors["white"]='FFFFFF';

		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
		->setCreator("Ana Carolina Mondragon Rangel")
		->setLastModifiedBy("Ana Carolina Mondragon Rangel")
		->setTitle("Programa Anual")
		->setSubject("Programa Anual")
		->setDescription("Programa Anual")
		->setKeywords("Programa Anual")
		->setCategory("Reporte");
		$spreadsheet->getActiveSheet()->setTitle("Resumen General"); //	$spreadsheet->getActiveSheet()->setTitle("Sujetos Fiscalizables");
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Tipo de Auditorias')->getColumnDimension('A')->setWidth(50);
		$sheet->setCellValue('B1', 'Total')->getColumnDimension('A')->setWidth(20);
		$sheet->getStyle('A1:B1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
		if ($datos["pagina1"]) {
			$posrow=2;
			$total=0;
			foreach ($datos["pagina1"] as $key => $value) {
				$sheet->setCellValue('A'.$posrow,$value->nombre);
				$sheet->setCellValue('B'.$posrow,$value->totalindividual);
				$total+=$value->totalindividual;
				$posrow++;
			}
			$sheet->setCellValue('A'.$posrow,"Total General");
			$sheet->setCellValue('B'.$posrow,$total);
		}

		$sheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet,'RG - Sujetos');
		$spreadsheet->addSheet($sheet);


		$sheet->setCellValue('A1', 'Sujetos Fiscalizables');
		$sheet->getColumnDimension('A')->setWidth(50);
		$sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
		$sheet->getStyle('A1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
		$sheet->setCellValue('A2', 'Total');
		$sheet->getStyle('A2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f0eeee');
		$sheet->getStyle('A2')->getAlignment()->setHorizontal('right');

		$posrow=3;
		foreach ($datos["pagina2"]["tipoente"] as $key => $value) {
			$sheet->setCellValue('A'.$posrow, $value->nombre);
			$posrow++;
		}
		$sheet->setCellValue('B1', 'Sujetos ');
		$sheet->getColumnDimension('B')->setWidth(20);
		$sheet->getStyle('B1')->getAlignment()->setHorizontal('center');
		$sheet->getStyle('B1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
		$sheet->getStyle('B2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f0eeee');
		$sheet->getStyle('B2:B7')->getAlignment()->setHorizontal('center');
		if ($datos["pagina2"]["tipoente"]) {
			$posrow=3;
			foreach ($datos["pagina2"]["tipoente"] as $key => $value) {
				$write=false;
				if ($datos["pagina2"]["suejto_entes"]) {
					foreach ($datos["pagina2"]["suejto_entes"] as $key => $otros) {
						if ($value->idcat_tipo_entes==$otros->idcat_tipo_entes) {
							$write=true;
							$sheet->setCellValue('B'.$posrow, $otros->sujetotipo);
							break;
						}
					}
				}
				if (!$write) {
					$sheet->setCellValue('B'.$posrow, "0");
				}
				$posrow++;
			}
		}
		$sheet->setCellValue ('B2','=SUM(B3:B'.$posrow.')');

		$sheet->setCellValue('C1', 'Auditorías');
		$sheet->getColumnDimension('C')->setWidth(20);
		$sheet->getStyle('C1')->getAlignment()->setHorizontal('center');
		$sheet->getStyle('C1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
		$sheet->getStyle('C2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f0eeee');
		$sheet->getStyle('C2:C7')->getAlignment()->setHorizontal('center');

		if ($datos["pagina2"]["tipoente"]) {
			$posrow=3;
			foreach ($datos["pagina2"]["tipoente"] as $key => $value) {
				$write=false;
				if ($datos["pagina2"]["otros_auditoria"]) {
					foreach ($datos["pagina2"]["otros_auditoria"] as $key => $otros) {
						if ($value->idcat_tipo_entes==$otros->idcat_tipo_entes) {
							$write=true;
							$sheet->setCellValue('C'.$posrow, $otros->total_auditorias);
							break;
						}
					}
				}
				if (!$write) {
					$sheet->setCellValue('C'.$posrow, "0");
				}
				$posrow++;
			}
		}
		$sheet->setCellValue ('C2','=SUM(C3:C'.$posrow.')');

		if ($datos["pagina2"]["tipoente"]) {
			foreach ($datos["pagina2"]["tipoente"] as $key => $value) {
				$datos["pagina3"]["poderes"]=$this->Programa->GetSujetoAuditoriasbyclase($aafiscal,$value->idcat_tipo_entes);
				$hojatemplate = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'RG - EFTA - '.getNameInitials($value->nombre));
				$spreadsheet->addSheet($hojatemplate);
				$hojatemplate->getColumnDimension('D')->setWidth(41);
				$hojatemplate->setCellValue('A1', 'Entes a Fiscalizar y Tipos de Audítorias');
				$hojatemplate->getStyle('A1')->getAlignment()->setHorizontal('center');
				$hojatemplate->getStyle('A1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
				$hojatemplate->mergeCells('A1:H1');
				$hojatemplate->setCellValue('A2', $value->nombre);
				$hojatemplate->getStyle('A2')->getAlignment()->setHorizontal('center');
				$hojatemplate->getStyle('A2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
				$hojatemplate->mergeCells('A2:H2');

				$hojatemplate->mergeCells('A3:H3');

				$hojatemplate->setCellValue('A4','No.');
				$hojatemplate->mergeCells('A4:A5');
				$hojatemplate->getStyle('A6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
				$hojatemplate->getStyle('A4')->getAlignment()->setHorizontal('center');
				$hojatemplate->getStyle('A7:A9')->getAlignment()->setHorizontal('center');

				$hojatemplate->setCellValue('B4', 'Sujetos Fiscalizables');
				$hojatemplate->mergeCells('B4:D5');
				$hojatemplate->getStyle('B4')->getAlignment()->setHorizontal('center');
				$hojatemplate->getStyle('B4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f1f1f1 ');

				$hojatemplate->setCellValue('B6', $value->nombre);
				$hojatemplate->mergeCells('B6:D6');
				$hojatemplate->getStyle('B6')->getAlignment()->setHorizontal('center');
				$hojatemplate->getStyle('B6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');

				$hojatemplate->getStyle('B4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('dfdfdf');

				$hojatemplate->setCellValue('E4', 'Tipos de Auditorías');
				$hojatemplate->getStyle('E4')->getAlignment()->setHorizontal('center');
				$hojatemplate->getStyle('A4:E4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('909497 ');

				$row=5;
				$col=5;
				$lastcol=5;
				$hojatemplate->mergeCells('E4:H4');
				if ($datos["tipos_auditoria"]) {
					foreach ($datos["tipos_auditoria"] as $key2 => $tipo_auditoria) {
						$hojatemplate->setCellValueByColumnAndRow($col, $row, $tipo_auditoria->nombre);
						$colname=\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
						$hojatemplate->getColumnDimension($colname)->setWidth(strlen($tipo_auditoria->nombre));
						$hojatemplate->getStyle($colname.$row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f1f1f1 ');
						$col++;
					}
					$hojatemplate->setCellValueByColumnAndRow($col, $row,"Total");
					$lastcol=$col;

				}
				$row=7;
				$col=5;
				$sujetoname="";
				if ($datos["pagina3"]["poderes"]) {
					$currentsujeto="";
					foreach ($datos["pagina3"]["poderes"] as $key => $value) {
						$colname=\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col);
						if ($currentsujeto=="") {
							$currentsujeto=$value->fk_clave;
							$hojatemplate->setCellValue("B".''.$row,$value->nombre_ente);
							$hojatemplate->mergeCells("B".$row.':D'.$row);
							$sujetoname=$value->nombre_ente;
						}
						if ($currentsujeto!=$value->fk_clave) {
							$currentsujeto=$value->fk_clave;
							$row++;
							$col=5;
							$hojatemplate->setCellValue("B".''.$row,$value->nombre_ente);
							$hojatemplate->mergeCells("B".$row.':D'.$row);
							if (strlen($sujetoname)<strlen($value->nombre_ente)) {
								$sujetoname=$value->nombre_ente;
							}
						}
						foreach ($datos["tipos_auditoria"] as $key => $tipoaudi) {
							if ($tipoaudi->idcat_tipo_auditoria==$value->cat_tipo_auditoria_idcat_tipo_auditoria) {
								$hojatemplate->setCellValueByColumnAndRow($col, $row,1);
								break;
							}
						}
						$col++;
					}
					$colname=\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($lastcol);
					$colnamecount=\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($lastcol-1);
					for ($i=7; $i <=$row ; $i++) {
						$hojatemplate->setCellValue ($colname.''.$i,'=SUM(E'.$i.': '.$colnamecount.''.$i.')');
						$hojatemplate->setCellValue('A'.$i, ($i-6));
					}
					$colname="E";
					$posrow=0;
					for ($i=0; $i < count($datos["tipos_auditoria"]); $i++) {
						if ($i!=0) {
							$colname=\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i+5);
						}
						$hojatemplate->setCellValue ($colname.'6','=SUM('.$colname.'7: '.$colname.''.$row.')');
						$posrow=$i+5;
					}
					$colname=\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($posrow+1);
					$hojatemplate->setCellValue ($colname.'6','=SUM('.$colname.'7: '.$colname.''.$row.')');
				}
			}
		}

		if ($datos["tipos_auditoria"]) {
			foreach ($datos["tipos_auditoria"] as $key => $tipo_auditoria) {
				$hojatitulo = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'EFA-'.$tipo_auditoria->clave);
				$spreadsheet->addSheet($hojatitulo);
				$hojatitulo->mergeCells("B2:J10");
				$hojatitulo->setCellValue('B2', 'Estrategias de Fiscalización de '.$tipo_auditoria->nombre);
				$hojatitulo->getStyle('B2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($colors["gray_black"]);
				$hojatitulo->getStyle('B2')->getAlignment()->setHorizontal('center');
				$hojatitulo->getStyle('B2')->getAlignment()->setVertical('center');
				$hojatitulo->getStyle('B2')->getFont()->getColor()->setARGB($colors["white"]);

				$hojatablas = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'EFA-'.$tipo_auditoria->clave.' Criterios Selección');
				$spreadsheet->addSheet($hojatablas);

				$hojatablas->setCellValue('A1', 'Entes a Fiscalizar');
				$hojatablas->getStyle('A1')->getAlignment()->setHorizontal('center');
				$hojatablas->getStyle('A1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
				$hojatablas->mergeCells('A1:C1');
				$hojatablas->setCellValue('A2', 'y Criterios de Selección');
				$hojatablas->getStyle('A2')->getAlignment()->setHorizontal('center');
				$hojatablas->getStyle('A2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
				$hojatablas->mergeCells('A2:C2');
				$hojatablas->mergeCells('A3:H3');
				$hojatablas->setCellValue('A4','No.');
				$hojatablas->getStyle('A4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($colors["gray_black"]);
				$hojatablas->getStyle('A5')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
				$hojatablas->getStyle('A')->getAlignment()->setHorizontal('center');

				$hojatablas->getStyle('A6:A8')->getAlignment()->setHorizontal('center');

				$hojatablas->setCellValue('B4', 'Sujetos Fiscalizables');
				$hojatablas->getColumnDimension('B')->setWidth(60);
				$hojatablas->getStyle('B4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($colors["gray_black"]);
				$hojatablas->getStyle('B4')->getAlignment()->setHorizontal('center');

				$hojatablas->setCellValue('C4', 'Criterios de Selección');
				$hojatablas->getColumnDimension('C')->setWidth(60);
				$hojatablas->getStyle('C4')->getAlignment()->setHorizontal('center');
				$hojatablas->getStyle('C4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($colors["gray_black"]);
				$hojatablas->getStyle('C5')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');



				$posrow=5;
				if ($datos["pagina2"]["tipoente"]) {
					foreach ($datos["pagina2"]["tipoente"] as $key2 => $tipoente) {
						$possujeto=1;
						$hojatablas->setCellValue('B'.$posrow,$tipoente->nombre);
						$hojatablas->getStyle('B'.$posrow)->getAlignment()->setHorizontal('center');
						$hojatablas->getStyle('B'.$posrow)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
						$hojatablas->getStyle('A'.$posrow)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
						$hojatablas->getStyle('C'.$posrow)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
						$datos["temp"]["sujetos"]=$this->Programa->GetSujetoAuditoriasbyclasetipoaudia($aafiscal,$tipoente->idcat_tipo_entes,$tipo_auditoria->idcat_tipo_auditoria);
						if ($datos["temp"]["sujetos"]) {
							$posrow++;
							foreach ($datos["temp"]["sujetos"] as $key3 => $suejto) {
								$hojatablas->setCellValue('A'.$posrow,$possujeto);
								$hojatablas->setCellValue('B'.$posrow, $suejto->nombre_ente);
								$hojatablas->setCellValue('C'.$posrow, $suejto->crisel);
								$possujeto++;
								$posrow++;
							}
						}else{
							$posrow++;
						}

					}
				}
				if ($tipo_auditoria->idcat_tipo_auditoria==2 || $tipo_auditoria->idcat_tipo_auditoria==3) {
					$resumeninversion = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'EFA-'.$tipo_auditoria->clave."MF Inversión");
					$spreadsheet->addSheet($resumeninversion);

					$resumeninversion->setCellValue('A1', 'Sujetos Fiscalizables');
					$resumeninversion->getColumnDimension('A')->setWidth(50);
					$resumeninversion->getStyle('A1')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('A1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
					$resumeninversion->setCellValue('A2', 'Total');
					$resumeninversion->getStyle('A2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f0eeee');
					$resumeninversion->getStyle('A2')->getAlignment()->setHorizontal('right');

					$posrow=3;
					foreach ($datos["pagina2"]["tipoente"] as $key => $value) {
						$resumeninversion->setCellValue('A'.$posrow, $value->nombre);
						$posrow++;
					}
					$resumeninversion->setCellValue('B1', 'Universo ');
					$resumeninversion->getColumnDimension('B')->setWidth(20);
					$resumeninversion->getStyle('B1')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('B1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
					$resumeninversion->getStyle('B2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f0eeee');

					$resumeninversion->setCellValue('C1', 'Muestra a fiscalizar');
					$resumeninversion->getColumnDimension('C')->setWidth(20);
					$resumeninversion->getStyle('C1')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('C1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
					$resumeninversion->getStyle('C2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f0eeee');

					$resumeninversion->setCellValue('D1', 'Alcance ');
					$resumeninversion->getColumnDimension('B')->setWidth(20);
					$resumeninversion->getStyle('D1')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('D1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
					$resumeninversion->getStyle('D2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f0eeee');
					$resumeninversion->getStyle('D2:D7')->getAlignment()->setHorizontal('center');

					$totaluniveros=0;
					if ($datos["pagina2"]["tipoente"]) {
						$posrow=3;
						foreach ($datos["pagina2"]["tipoente"] as $key => $tiposubente) {

							$write=false;
							$datos["temp"]["sujetos"]=$this->Programa->GetSujetoAuditoriasbyclasetipoaudia($aafiscal,$tiposubente->idcat_tipo_entes,$tipo_auditoria->idcat_tipo_auditoria);
							if ($datos["temp"]["sujetos"]) {
								$universo=0;
								$muestra=0;
								$alcance=0;
								$innercount=0;
								foreach ($datos["temp"]["sujetos"] as $key => $sujeto_datos) {
									$write=true;
									$universo+=$sujeto_datos->universo;
									$muestra+=$sujeto_datos->muestra;
									$alcance+=doubleval($sujeto_datos->alcance);
									$innercount++;
									break;
								}
								$resumeninversion->setCellValue('B'.$posrow, $universo);
								$resumeninversion->setCellValue('C'.$posrow, $muestra);
								if ($innercount>0) {
									$resumeninversion->setCellValue('D'.$posrow,((($alcance)/$innercount))/100);
								}else{
									$resumeninversion->setCellValue('D'.$posrow,"0");
								}
							}
							if (!$write) {
								$resumeninversion->setCellValue('B'.$posrow, "0");
								$resumeninversion->setCellValue('C'.$posrow, "0");
								$resumeninversion->setCellValue('D'.$posrow, "0");
							}
							$posrow++;
							$totaluniveros++;
						}
					}
					$resumeninversion->setCellValue ('B2','=SUM(B3:B'.($posrow-1).')');
					$resumeninversion->setCellValue ('C2','=SUM(C3:C'.($posrow-1).')');
					$resumeninversion->setCellValue ('D2','=(SUM(D3:D'.($posrow-1).')/'.$totaluniveros.')');

					$resumeninversion->getStyle('B2')->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
					$resumeninversion->getStyle('C2')->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
					$resumeninversion->getStyle('D2:D'.$posrow)->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);
					$resumeninversion->getStyle('C2:C'.$posrow)->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('B2:B'.$posrow)->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('D2:D'.$posrow)->getAlignment()->setHorizontal('center');
					/*    */
					$resumeninversion = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'EFA-'.$tipo_auditoria->clave."MF Inversión INDV");
					$spreadsheet->addSheet($resumeninversion);
					$resumeninversion->setCellValue('A1', 'Muestra a Fiscalizar por '.$tipo_auditoria->nombre);
					$resumeninversion->getStyle('A1')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('A1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
					$resumeninversion->mergeCells('A1:E1');
					$resumeninversion->setCellValue('A2', '');
					$resumeninversion->getStyle('A2')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('A2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
					$resumeninversion->mergeCells('A2:E2');
					$resumeninversion->mergeCells('A3:E3');
					$resumeninversion->getStyle('A6:A8')->getAlignment()->setHorizontal('center');
					$configheader = array(
						array(
							'cell' =>"A",
							'title' =>"No.",
							'alinghorizontal'=>"center",
							'width'=>0,
						),
						array(
							'cell' =>"B",
							'title' =>"Sujetos Fiscalizables",
							'alinghorizontal'=>"center",
							'width'=>60,
						),
						array(
							'cell' =>"C",
							'title' =>"Universo $",
							'alinghorizontal'=>"center",
							'width'=>22,
						),
						array(
							'cell' =>"D",
							'title' =>"Muestra a Fiscalizar  $",
							'alinghorizontal'=>"center",
							'width'=>22,
						),
						array(
							'cell' =>"E",
							'title' =>"Alcance",
							'alinghorizontal'=>"center",
							'width'=>22,
						),
					);

					for ($i=0; $i < count($configheader); $i++) {
						$resumeninversion->setCellValue( $configheader[$i]["cell"].'4', $configheader[$i]["title"]);
						if($configheader[$i]["width"]>0) {
							$resumeninversion->getColumnDimension($configheader[$i]["cell"])->setWidth($configheader[$i]["width"]);
						}
						$resumeninversion->getStyle($configheader[$i]["cell"].'4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($colors["gray_black"]);
						if($configheader[$i]["alinghorizontal"]!="") {
							$resumeninversion->getStyle($configheader[$i]["cell"].'4')->getAlignment()->setHorizontal($configheader[$i]["alinghorizontal"]);
						}
						$resumeninversion->getStyle($configheader[$i]["cell"].'5')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
					}

					$posrow=6;
					$postitles=[];
					if ($datos["pagina2"]["tipoente"]) {
						foreach ($datos["pagina2"]["tipoente"] as $key2 => $tipoente) {
							$possujeto=1;
							$resumeninversion->setCellValue('B'.$posrow,$tipoente->nombre);
							$resumeninversion->getStyle('B'.$posrow)->getAlignment()->setHorizontal('center');
							for ($i=0; $i < count($configheader); $i++) {
								$resumeninversion->getStyle($configheader[$i]["cell"].''.$posrow)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
							}
							$datos["temp"]["sujetos"]=$this->Programa->GetSujetoAuditoriasbyclasetipoaudia($aafiscal,$tipoente->idcat_tipo_entes,$tipo_auditoria->idcat_tipo_auditoria);
							$postitles[]=$posrow;
							if ($datos["temp"]["sujetos"]) {
								$countsujetos=count($datos["temp"]["sujetos"]);
								$resumeninversion->setCellValue ('C'.$posrow,'=SUM(C'.($posrow+1).':C'.($posrow+$countsujetos).')');
								$resumeninversion->setCellValue ('D'.$posrow,'=SUM(D'.($posrow+1).':D'.($posrow+$countsujetos).')');
								$resumeninversion->setCellValue ('E'.$posrow,'=(SUM(E'.($posrow+1).':E'.($posrow+$countsujetos).'))/'.$countsujetos);
								$posrow++;
								foreach ($datos["temp"]["sujetos"] as $key3 => $suejto) {
									$resumeninversion->setCellValue('A'.$posrow,$possujeto);
									$resumeninversion->setCellValue('B'.$posrow, $suejto->nombre_ente);
									$resumeninversion->setCellValue('C'.$posrow, $suejto->universo);
									$resumeninversion->setCellValue('D'.$posrow, $suejto->muestra);
									$resumeninversion->setCellValue('E'.$posrow, (doubleval($suejto->alcance))/100);
									$possujeto++;
									$posrow++;
								}
							}else{
								$resumeninversion->setCellValue('C'.$posrow, 0);
								$resumeninversion->setCellValue('D'.$posrow, 0);
								$resumeninversion->setCellValue('E'.$posrow, 0);
								$posrow++;
							}

						}
						$resumeninversion->getStyle('E5:E'.$posrow)->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);
						$resumeninversion->getStyle('C5')->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
						$resumeninversion->getStyle('D5')->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
						$cellstosum["C"]="";
						$cellstosum["D"]="";
						$cellstosum["E"]="";
						for ($i=0; $i <count($postitles); $i++) {
							if ($i< count($postitles)-1) {
								$cellstosum["C"].="C".$postitles[$i]."+";
								$cellstosum["D"].="D".$postitles[$i]."+";
								$cellstosum["E"].="E".$postitles[$i]."+";
							}else{
								$cellstosum["C"].="C".$postitles[$i]."";
								$cellstosum["D"].="D".$postitles[$i]."";
								$cellstosum["E"].="E".$postitles[$i]."";
							}
						}
						$resumeninversion->setCellValue ('C5','=SUM('.$cellstosum["C"].')');
						$resumeninversion->setCellValue ('D5','=SUM('.$cellstosum["D"].')');
						$resumeninversion->setCellValue ('E5','=(SUM('.$cellstosum["E"].'))/'.count($postitles));
					}
					/* FIN */
				}
				if ($tipo_auditoria->idcat_tipo_auditoria==3) {
					$resumeninversion = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'EFA-'.$tipo_auditoria->clave."MF Obra");
					$spreadsheet->addSheet($resumeninversion);
					$resumeninversion->setCellValue('A1', 'Sujetos Fiscalizables');
					$resumeninversion->getColumnDimension('A')->setWidth(50);
					$resumeninversion->getStyle('A1')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('A1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
					$resumeninversion->setCellValue('A2', 'Total');
					$resumeninversion->getStyle('A2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f0eeee');
					$resumeninversion->getStyle('A2')->getAlignment()->setHorizontal('right');

					$posrow=3;
					foreach ($datos["pagina2"]["tipoente"] as $key => $value) {
						$resumeninversion->setCellValue('A'.$posrow, $value->nombre);
						$posrow++;
					}
					$resumeninversion->setCellValue('B1', 'Universo ');
					$resumeninversion->getColumnDimension('B')->setWidth(20);
					$resumeninversion->getStyle('B1')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('B1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
					$resumeninversion->getStyle('B2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f0eeee');

					$resumeninversion->setCellValue('C1', 'Muestra a fiscalizar');
					$resumeninversion->getColumnDimension('C')->setWidth(20);
					$resumeninversion->getStyle('C1')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('C1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
					$resumeninversion->getStyle('C2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f0eeee');

					$resumeninversion->setCellValue('D1', 'Alcance ');
					$resumeninversion->getColumnDimension('B')->setWidth(20);
					$resumeninversion->getStyle('D1')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('D1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b8b4b4');
					$resumeninversion->getStyle('D2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f0eeee');
					$resumeninversion->getStyle('D2:D7')->getAlignment()->setHorizontal('center');
					$totaluniveros=0;
					if ($datos["pagina2"]["tipoente"]) {
						$posrow=3;
						foreach ($datos["pagina2"]["tipoente"] as $key => $tiposubente) {
							$write=false;
							$datos["temp"]["sujetos"]=$this->Programa->GetSujetoAuditoriasbyclasetipoaudia($aafiscal,$tiposubente->idcat_tipo_entes,$tipo_auditoria->idcat_tipo_auditoria);
							if ($datos["temp"]["sujetos"]) {
								$universo=0;
								$muestra=0;
								$alcance=0;
								$innercount=0;
								foreach ($datos["temp"]["sujetos"] as $key => $sujeto_datos) {
									if ($sujeto_datos->fk_claseente==$tiposubente->idcat_tipo_entes) {
										$write=true;
										$universo+=$sujeto_datos->universox;
										$muestra+=$sujeto_datos->muestrax;
										$alcance+=doubleval($sujeto_datos->alcancex);
										$innercount++;
										break;
									}
								}
								$resumeninversion->setCellValue('B'.$posrow, $universo);
								$resumeninversion->setCellValue('C'.$posrow, $muestra);
								if ($innercount>0) {
									$resumeninversion->setCellValue('D'.$posrow,(($alcance)/$innercount)/100);
								}else{
									$resumeninversion->setCellValue('D'.$posrow,"0");
								}
							}
							if (!$write) {
								$resumeninversion->setCellValue('B'.$posrow, "0");
								$resumeninversion->setCellValue('C'.$posrow, "0");
								$resumeninversion->setCellValue('D'.$posrow, "0");
							}
							$posrow++;
							$totaluniveros++;
						}
					}
					$resumeninversion->setCellValue ('B2','=SUM(B3:B'.($posrow-1).')');
					$resumeninversion->setCellValue ('C2','=SUM(C3:C'.($posrow-1).')');
					$resumeninversion->setCellValue ('D2','=(SUM(D3:D'.($posrow-1).')/'.$totaluniveros.')');
					/* $sheet->getStyle("A1")->getNumberFormat()->setFormatCode('0.00');  */
					/* $sheet->getStyle("A1")->getNumberFormat()->setFormatCode('### ### ### ##0.00'); */
					$resumeninversion->getStyle('B2')->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
					$resumeninversion->getStyle('C2')->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
					$resumeninversion->getStyle('D2:D'.$posrow)->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);
					$resumeninversion->getStyle('C2:C'.$posrow)->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('B2:B'.$posrow)->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('D2:D'.$posrow)->getAlignment()->setHorizontal('center');

					$resumeninversion = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'EFA-'.$tipo_auditoria->clave."MF Obra INDV");
					$spreadsheet->addSheet($resumeninversion);
					$resumeninversion->setCellValue('A1', 'Muestra a Fiscalizar');
					$resumeninversion->getStyle('A1')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('A1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
					$resumeninversion->mergeCells('A1:E1');
					$resumeninversion->setCellValue('A2', '');
					$resumeninversion->getStyle('A2')->getAlignment()->setHorizontal('center');
					$resumeninversion->getStyle('A2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
					$resumeninversion->mergeCells('A2:E2');
					$resumeninversion->mergeCells('A3:E3');
					$resumeninversion->getStyle('A6:A8')->getAlignment()->setHorizontal('center');

					$configheader = array(
						array(
							'cell' =>"A",
							'title' =>"No.",
							'alinghorizontal'=>"center",
							'width'=>0,
						),
						array(
							'cell' =>"B",
							'title' =>"Sujetos Fiscalizables",
							'alinghorizontal'=>"center",
							'width'=>60,
						),
						array(
							'cell' =>"C",
							'title' =>"Universo",
							'alinghorizontal'=>"center",
							'width'=>22,
						),
						array(
							'cell' =>"D",
							'title' =>"Muestra a Fiscalizar",
							'alinghorizontal'=>"center",
							'width'=>22,
						),
						array(
							'cell' =>"E",
							'title' =>"Alcance",
							'alinghorizontal'=>"center",
							'width'=>22,
						),
					);

					for ($i=0; $i < count($configheader); $i++) {
						$resumeninversion->setCellValue( $configheader[$i]["cell"].'4', $configheader[$i]["title"]);
						if($configheader[$i]["width"]>0) {
							$resumeninversion->getColumnDimension($configheader[$i]["cell"])->setWidth($configheader[$i]["width"]);
						}
						$resumeninversion->getStyle($configheader[$i]["cell"].'4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($colors["gray_black"]);
						if($configheader[$i]["alinghorizontal"]!="") {
							$resumeninversion->getStyle($configheader[$i]["cell"].'4')->getAlignment()->setHorizontal($configheader[$i]["alinghorizontal"]);
						}
						$resumeninversion->getStyle($configheader[$i]["cell"].'5')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
					}

					$posrow=6;
					$postitles=[];
					if ($datos["pagina2"]["tipoente"]) {
						foreach ($datos["pagina2"]["tipoente"] as $key2 => $tipoente) {
							$possujeto=1;
							$resumeninversion->setCellValue('B'.$posrow,$tipoente->nombre);
							$resumeninversion->getStyle('B'.$posrow)->getAlignment()->setHorizontal('center');
							for ($i=0; $i < count($configheader); $i++) {
								$resumeninversion->getStyle($configheader[$i]["cell"].''.$posrow)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('e8e1e1');
							}
							$datos["temp"]["sujetos"]=$this->Programa->GetSujetoAuditoriasbyclasetipoaudia($aafiscal,$tipoente->idcat_tipo_entes,$tipo_auditoria->idcat_tipo_auditoria);
							$postitles[]=$posrow;
							if ($datos["temp"]["sujetos"]) {
								$countsujetos=count($datos["temp"]["sujetos"]);
								$resumeninversion->setCellValue ('C'.$posrow,'=SUM(C'.($posrow+1).':C'.($posrow+$countsujetos).')');
								$resumeninversion->setCellValue ('D'.$posrow,'=SUM(D'.($posrow+1).':D'.($posrow+$countsujetos).')');
								$resumeninversion->setCellValue ('E'.$posrow,'=(SUM(E'.($posrow+1).':E'.($posrow+$countsujetos).'))/'.$countsujetos);
								$posrow++;
								foreach ($datos["temp"]["sujetos"] as $key3 => $suejto) {
									$resumeninversion->setCellValue('A'.$posrow,$possujeto);
									$resumeninversion->setCellValue('B'.$posrow, $suejto->nombre_ente);
									$resumeninversion->setCellValue('C'.$posrow, $suejto->universox);
									$resumeninversion->setCellValue('D'.$posrow, $suejto->muestrax);
									$resumeninversion->setCellValue('E'.$posrow, (doubleval($suejto->alcancex))/100);
									$possujeto++;
									$posrow++;
								}
							}else{
								$resumeninversion->setCellValue('C'.$posrow, 0);
								$resumeninversion->setCellValue('D'.$posrow, 0);
								$resumeninversion->setCellValue('E'.$posrow, 0);
								$posrow++;
							}

						}
						$resumeninversion->getStyle('E5:E'.$posrow)->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE);
						$cellstosum["C"]="";
						$cellstosum["D"]="";
						$cellstosum["E"]="";
						for ($i=0; $i <count($postitles); $i++) {
							if ($i< count($postitles)-1) {
								$cellstosum["C"].="C".$postitles[$i]."+";
								$cellstosum["D"].="D".$postitles[$i]."+";
								$cellstosum["E"].="E".$postitles[$i]."+";
							}else{
								$cellstosum["C"].="C".$postitles[$i]."";
								$cellstosum["D"].="D".$postitles[$i]."";
								$cellstosum["E"].="E".$postitles[$i]."";
							}
						}
						$resumeninversion->setCellValue ('C5','=SUM('.$cellstosum["C"].')');
						$resumeninversion->setCellValue ('D5','=SUM('.$cellstosum["D"].')');
						$resumeninversion->setCellValue ('E5','=(SUM('.$cellstosum["E"].'))/'.count($postitles));
					}
				}
			}
		}

		$spreadsheet->setActiveSheetIndex(0);
		$writer = new Xlsx($spreadsheet);
		$filename = 'PROGRAMA DE AUDITORIA ANUAL -'.$aafiscal;
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
		header('Cache-Control: max-age=0');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header("Access-Control-Allow-Methods: GET, OPTIONS,POST");
		header("Access-Control-Allow-Headers: Origin, Authorization, X-Requested-With, Content-Type, Accept,Accept-Encoding , Content-Length,,Authorization");
		$writer->save('php://output');
	}
}
