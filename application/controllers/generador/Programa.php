<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\Element\Field;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\SimpleType\TblWidth;

class Programa extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper(array('url', 'form','headers','jwt','auth','money','dates','headersoffices'));
		$this->load->model(array('planeacion/Planeacion','Direcciones'));
	}
	public function index()
	{
		HeaderJson();
		$arrayName["system"]="sigfi";
		echo json_encode($arrayName);
	}
	public function Programaespecifico()
	{
		HeaderJson();
		if ($this->input->method()=="post" ) {
			$datosdelpost = json_decode(file_get_contents('php://input'));
			$documentconfig["bold"]= array('bold' => true, 'align' => 'center', 'size'=>8);
			$documentconfig["normal_text_l"]= array('bold' => false, 'align' => 'left', 'size'=>8);
			$documentconfig["normal_text_c"]= array('bold' => false, 'align' => 'center', 'size'=>8);

			$documentconfig["bold2"]= array('bold' => true, 'align' => 'center', 'size'=>12);
			$documentconfig["normal_text"]= array('bold' => false, 'align' => 'right', 'size'=>8);

			try {
				$templateProcessor = new TemplateProcessor('formatos/planeacion/programaespecifico_general.docx');
				$templateProcessor->setValue('anofiscal',$datosdelpost->aafiscal);
				$templateProcessor->setValue('numero',$datosdelpost->numeroauditoria);
				$templateProcessor->setValue('entenombre',$datosdelpost->sujeto->nombre_ente);
				if ($datosdelpost->tpfiscal=="C") {
					$templateProcessor->setValue('tipofis',"Cuenta Publica");
				}
				if ($datosdelpost->tpfiscal=="S") {
					$templateProcessor->setValue('tipofis',"Auditoria Especial");
				}

				if ($datosdelpost->tipoaudidata) {
					$temp1=1;
					$templateProcessor->cloneRow('tipoauditoria',count($datosdelpost->tipoaudidata));
					$temppospersonal=1;
					$totalpersonal=0;
					$personal=[];
					foreach ($datosdelpost->tipoaudidata as $key => $auditipo) {
						$templateProcessor->setValue('tipoauditoria#'.$temp1,$auditipo->cat_tipo_auditoria_nombre);
						$templateProcessor->setValue('tipoaudir#'.$temp1," ");
						$templateProcessor->setValue('tipoaudepa#'.$temp1," ");
						if ($auditipo->personalauditor) {
							$personal[]=$auditipo->personalauditor;
							$totalpersonal+=count($auditipo->personalauditor);
						}
						$temp1++;

					}
					$templateProcessor->cloneRow('pernombre',$totalpersonal);
					if ($personal) {
						$currentper=0;
						for ($i=0; $i < count($personal); $i++) {
							foreach ($personal[$i] as $key => $personaldata) {
								$templateProcessor->setValue('pernombre#'.$temppospersonal,$personaldata->nombre." ".$personaldata->nombre." ".$personaldata->nombre);
								$templateProcessor->setValue('puesto#'.$temppospersonal,$personaldata->nombre_puesto);
								$templateProcessor->setValue('iniciales#'.$temppospersonal,$personaldata->iniciales);
								$temppospersonal++;
							}
						}
					}



					$table = new Table(
						array(
							'borderSize' => 1,
							'width' => 0,
							'unit' => TblWidth::TWIP,
							'layout'=>\PhpOffice\PhpWord\Style\Table::LAYOUT_FIXED,
						)
					);
					$cellRowSpan = array('vMerge' => 'restart');
					$cellRowContinue = array('vMerge' => 'continue');
					$cellColSpan = array('gridSpan' => 2);

					$table->addRow(500,array('tblHeader' => true));
					$table->addCell(2000,array('gridSpan' => 9))->addText("PROGRAMA ESPECÍFICO DE ACTIVIDADES",$documentconfig["bold2"]);

					$table->addRow(500,array('tblHeader' => true));
					$table->addCell(1000, $cellRowSpan)->addText("PROCEDIMIENTOS DE AUDITORÍA/ACTIVIDADES",$documentconfig["normal_text"]);
					$table->addCell(500, $cellColSpan)->addText("FECHA PROGRAMADA",$documentconfig["normal_text"]);
					$table->addCell(500, $cellColSpan)->addText("FECHA REAL",$documentconfig["normal_text"]);
					$table->addCell(500, $cellColSpan)->addText("DÍAS EJECUCIÓN",$documentconfig["normal_text"]);
					$table->addCell(900, $cellRowSpan)->addText("ELABORA",$documentconfig["normal_text"]);
					$table->addCell(1000, $cellRowSpan)->addText("SUPERVISA",$documentconfig["normal_text"]);

					$table->addRow(500,array('tblHeader' => true));
					$table->addCell(null, $cellRowContinue);
					$table->addCell(100)->addText("INICIO",$documentconfig["normal_text"]);
					$table->addCell(100)->addText("TERMINO",$documentconfig["normal_text"]);
					$table->addCell(100)->addText("INICIO",$documentconfig["normal_text"]);
					$table->addCell(100)->addText("TERMINO",$documentconfig["normal_text"]);
					$table->addCell(300)->addText("PROG",$documentconfig["normal_text"]);
					$table->addCell(300)->addText("REAL",$documentconfig["normal_text"]);
					$table->addCell(null, $cellRowContinue);
					$table->addCell(null, $cellRowContinue);



					for ($i=0; $i < count($datosdelpost->tipoaudidata); $i++) {
						$table->addRow();
						$table->addCell(1710)->addText($datosdelpost->tipoaudidata[$i]->cat_tipo_auditoria_nombre,$documentconfig["bold"]);
						$table->addCell(1100)->addText(' ',$documentconfig["normal_text_l"]);
						$table->addCell(1080)->addText(' ',$documentconfig["normal_text_l"]);
						$table->addCell(1080)->addText(' ',$documentconfig["normal_text_l"]);
						$table->addCell(1090)->addText(' ',$documentconfig["normal_text_l"]);
						$table->addCell(700)->addText(' ',$documentconfig["normal_text_l"]);
						$table->addCell(580)->addText(' ',$documentconfig["normal_text_l"]);
						$table->addCell(750)->addText(' ',$documentconfig["normal_text_l"]);
						$table->addCell(750)->addText(' ',$documentconfig["normal_text_l"]);

						$dataserver["procedimeintos"]=$this->Planeacion->GetProcedimientosAuditoria($datosdelpost->tipoaudidata[$i]->idprogramaauditoriatipo);
						if ($dataserver["procedimeintos"]) {
							foreach ($dataserver["procedimeintos"] as $key => $v_procedimientos) {
								$table->addRow();
								$table->addCell(1710)->addText("Procedimienti",$documentconfig["normal_text_l"]);
								$table->addCell(1100)->addText(getFormatDate($v_procedimientos->p_fechainicio),$documentconfig["normal_text_l"]);
								$table->addCell(1080)->addText(getFormatDate($v_procedimientos->p_fechatermino),$documentconfig["normal_text_l"]);
								$table->addCell(1080)->addText(getFormatDate($v_procedimientos->r_fechainicio),$documentconfig["normal_text_l"]);
								$table->addCell(1090)->addText(getFormatDate($v_procedimientos->r_fechatermino),$documentconfig["normal_text_l"]);
								$table->addCell(700)->addText($v_procedimientos->p_diasejecucion,$documentconfig["normal_text_l"]);
								$table->addCell(580)->addText($v_procedimientos->r_diasejecucion,$documentconfig["normal_text_l"]);
								$temp_auditores="";
								$dataserver["procedimiento_personal"]=$this->Planeacion->GetProcedimientosAuditoriaPersonal($v_procedimientos->idauditoriaprocedimientos);
								if ($dataserver["procedimiento_personal"]) {
									foreach ($dataserver["procedimiento_personal"] as $key => $v_actividad_personal) {
										$temp_auditores.=$v_actividad_personal->iniciales."\n";
									}
								}
								$table->addCell(750)->addText($temp_auditores,$documentconfig["normal_text_l"]);
								$table->addCell(750)->addText($v_procedimientos->iniciales,$documentconfig["normal_text_l"]);
								$dataserver["actividades"]=$this->Planeacion->GetActividadesProcedimientosAuditoria($v_procedimientos->idauditoriaprocedimientos);
								if ($dataserver["actividades"]) {
									foreach ($dataserver["actividades"] as $key => $v_actividad) {
										$table->addRow();
										$table->addCell(1710)->addText($v_actividad->actividad,$documentconfig["normal_text_l"]);
										$table->addCell(1100)->addText(getFormatDate($v_actividad->p_fechainicio),$documentconfig["normal_text_l"]);
										$table->addCell(1080)->addText(getFormatDate($v_actividad->p_fechatermino),$documentconfig["normal_text_l"]);
										$table->addCell(1080)->addText(getFormatDate($v_actividad->r_fechainicio),$documentconfig["normal_text_l"]);
										$table->addCell(1090)->addText(getFormatDate($v_actividad->r_fechatermino),$documentconfig["normal_text_l"]);
										$table->addCell(700)->addText($v_actividad->p_diasejecucion,$documentconfig["normal_text_l"]);
										$table->addCell(580)->addText($v_actividad->r_diasejecucion,$documentconfig["normal_text_l"]);
										$temp_auditores="";
										$dataserver["actividades_personal"]=$this->Planeacion->GetPersonalActividad($v_actividad->idactividad);
										if ($dataserver["actividades_personal"]) {
											foreach ($dataserver["actividades_personal"] as $key => $v_actividad_personal) {
												$temp_auditores.=$v_actividad_personal->iniciales."\n";
											}
										}
										$table->addCell(750)->addText($temp_auditores,$documentconfig["normal_text_l"]);
										$table->addCell(750)->addText($v_actividad->iniciales,$documentconfig["normal_text_l"]);

									}
								}
								$dataserver["procedimiento_personal"]=false;
								$dataserver["actividades_personal"]=false;
							}
						}

						// for ($e=0; $e < 5; $e++) {
						// 	$table->addRow();
						// 	$table->addCell(1710)->addText("Procedimienti".$e,$documentconfig["normal_text_l"]);
						// 	$table->addCell(1100)->addText(' ',$documentconfig["normal_text_l"]);
						// 	$table->addCell(1080)->addText(' ',$documentconfig["normal_text_l"]);
						// 	$table->addCell(1080)->addText(' ',$documentconfig["normal_text_l"]);
						// 	$table->addCell(1090)->addText(' ',$documentconfig["normal_text_l"]);
						// 	$table->addCell(700)->addText(' ',$documentconfig["normal_text_l"]);
						// 	$table->addCell(580)->addText(' ',$documentconfig["normal_text_l"]);
						// 	$temp_auditores="";
						// 	for ($x=0; $x < 2; $x++) {
						// 		//estre for deberemos concatenar los servidores publicos que seran sacados de la tabla personal actividad.
						// 		$temp_auditores.="Auditor ".$x."\n";
						// 	}
						// 	$table->addCell(750)->addText($temp_auditores);
						// 	$table->addCell(750)->addText(' ');
						// 	for ($x2=0; $x2 < 10; $x2++) {
						// 		$table->addRow();
						// 		$table->addCell(1710)->addText("Actividad".$x2,$documentconfig["normal_text_l"]);
						// 		$table->addCell(1100)->addText(' ',$documentconfig["normal_text_l"]);
						// 		$table->addCell(1080)->addText(' ',$documentconfig["normal_text_l"]);
						// 		$table->addCell(1080)->addText(' ',$documentconfig["normal_text_l"]);
						// 		$table->addCell(1090)->addText(' ',$documentconfig["normal_text_l"]);
						// 		$table->addCell(700)->addText(' ',$documentconfig["normal_text_l"]);
						// 		$table->addCell(580)->addText(' ',$documentconfig["normal_text_l"]);
						// 		$temp_auditores_actividad="";
						// 		for ($x=0; $x < 1; $x++) {
						// 			//estre for deberemos concatenar los servidores publicos que seran sacados de la tabla personal actividad.
						// 			$temp_auditores_actividad.="Auditor ".$x."\n";
						// 		}
						// 		$table->addCell(750)->addText($temp_auditores_actividad);
						// 		$table->addCell(750)->addText(' ');
						// 	}
						// }

					}
					$templateProcessor->setComplexBlock('table', $table);

				}
				HeaderWORD("Programa Aududitoria");
				$templateProcessor->saveAs("php://output");
			} catch (Exception $e) {
				$data["message"]=$e->getMessage();
				echo json_encode($data["message"]);
			}
		}

	}
	public function Test4($value='')
	{
		HeaderJson();
		$datosdelpost = json_decode(file_get_contents('php://input'));
		$datos["datos"]=$datosdelpost;
		$this->load->view("testword",$datos);
		header('Content-Type: application/msword');
		header('Content-disposition: filename=mydocument.doc');
	}
	public function Test5($value='')
	{
		HeaderJson();
		$this->load->library('Word');
		$TBS = new Word();
		$TBS->LoadTemplate('formatos/test/mydocument.htm');
		$baseurl = base_url();
		$TBS->Show(DOWNLOAD);
	}
	/**
	*
	*
	* @param null
	*
	* @return void
	*/
	public function GenerateXMLPHP($content='')
	{
		$phpWord = new PhpWord();
		$section = $phpWord->addSection();
		$doc = new DOMDocument();
		$doc->loadHTML($content);
		$doc->saveHTML();
		\PhpOffice\PhpWord\Shared\Html::addHtml($section, $doc->saveHtml(),true);
		$xmlWriter = new XMLWriter(XMLWriter::STORAGE_MEMORY, './', WordSettings::hasCompatibility());
		$containerWriter = new Container($xmlWriter, $section);
		return $containerWriter->write();
	}
}
