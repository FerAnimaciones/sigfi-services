<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper(array('url', 'form','headers','jwt','auth'));
		$this->load->model(array('Infodocumentacion'));
	}
	public function index()
	{
		HeaderJson();
		$arrayName["system"]="sigfi";
		echo json_encode($arrayName);
	}
	public function Ordenauditoriainvd($id="",$value='')
	{
		HeaderJson();
		$data= array();
		if ($this->input->method()=="get") {
			$data=authtoken(apache_request_headers(),$data,$this->config);
			if ($data["authorization"]) {
				$data["documento"]=$this->Infodocumentacion->GetOrdenAuditoria($id);
				$body="";
				if ($data) {
					$body = $this->load->view('reportes/documentacion/ordenaduitoria',$data,TRUE);
				}
				$this->load->library('PDFDocumentacion');
				$resolution = array(215.9,279.4);
				$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
				$pdf->SetTitle('ORDEN AUDITORIA');
				$pdf->SetSubject('SIGFI');
				$pdf->SetKeywords('SIGFI');
				$pdf->SetPrintHeader(true);
				$pdf->SetPrintFooter(true);
				$pdf->SetAutoPageBreak(TRUE, 0);

				$pdf->AddPage("P",$resolution);
				$pdf->writeHTML($body, true, false, true, false, '');
				$pdf->Output("TEST.pdf", 'D');
			}else {
				E401PDF();
			}
		}

	}
	public function Solventacioniipeipinvd($id="")
	{
		HeaderJson();
		$data["documento"]=$this->Infodocumentacion->GetSolventacion($id);
		$body="";
		if ($data) {
			$body = $this->load->view('reportes/documentacion/solventacion',$data,TRUE);
		}
		$this->load->library('PDFDocumentacion');
		$resolution = array(215.9,279.4);
		$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
		$pdf->SetTitle('SOLVENTACION IIP EIP');
		$pdf->SetSubject('SIGFI');
		$pdf->SetKeywords('SIGFI');
		$pdf->SetPrintHeader(true);
		$pdf->SetPrintFooter(true);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->AddPage("P",$resolution);
		$pdf->writeHTML($body, true, false, true, false, '');
		$pdf->Output("TEST.pdf", 'D');
	}
	public function Solicituddoccominvd($id="",$value='')
	{
		HeaderJson();
		$data["documento"]=$this->Infodocumentacion->GetSolicitudDocCom($id);
		$body="";
		if ($data) {
			$body = $this->load->view('reportes/documentacion/solicituddoccom',$data,TRUE);
		}
		$this->load->library('PDFDocumentacion');
		$resolution = array(215.9,279.4);
		$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
		$pdf->SetTitle('SOLICITUD DOCUMENTACION COMPLEMENTARIA');
		$pdf->SetSubject('SIGFI');
		$pdf->SetKeywords('SIGFI');
		$pdf->SetPrintHeader(true);
		$pdf->SetPrintFooter(true);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->AddPage("P",$resolution);
		$pdf->writeHTML($body, true, false, true, false, '');
		$pdf->Output("TEST.pdf", 'D');
	}
	public function Cuentapublicacpinvd($id="",$value='')
	{
		HeaderJson();
		$data["documento"]=$this->Infodocumentacion->GetCuentaPublicaCP($id);
		$body="";
		if ($data) {
			$body = $this->load->view('reportes/documentacion/cuentapublicacp',$data,TRUE);
		}
		$this->load->library('PDFDocumentacion');
		$resolution = array(215.9,279.4);
		$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
		$pdf->SetTitle('CUENTA PUBLICA CP');
		$pdf->SetSubject('SIGFI');
		$pdf->SetKeywords('SIGFI');
		$pdf->SetPrintHeader(true);
		$pdf->SetPrintFooter(true);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->AddPage("P",$resolution);
		$pdf->writeHTML($body, true, false, true, false, '');
		$pdf->Output("TEST.pdf", 'D');
	}
	public function Informeavancegfinvd($id="",$value='')
	{
		HeaderJson();
		$data["documento"]=$this->Infodocumentacion->GetInformeAvanceGF($id);
		$body="";
		if ($data) {
			$body = $this->load->view('reportes/documentacion/informeavancegf',$data,TRUE);
		}
		$this->load->library('PDFDocumentacion');
		$resolution = array(215.9,279.4);
		$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
		$pdf->SetTitle('INFORME AVANCE GESTION FINANCIERA');
		$pdf->SetSubject('SIGFI');
		$pdf->SetKeywords('SIGFI');
		$pdf->SetPrintHeader(true);
		$pdf->SetPrintFooter(true);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->AddPage("P",$resolution);
		$pdf->writeHTML($body, true, false, true, false, '');
		$pdf->Output("TEST.pdf", 'D');
	}
	public function Atencionreciidieinvd($id="",$value='')
	{
		HeaderJson();
		$data["documento"]=$this->Infodocumentacion->GetAtencionRecIIDIE($id);
		$body="";
		if ($data) {
			$body = $this->load->view('reportes/documentacion/atencionreciidie',$data,TRUE);
		}
		$this->load->library('PDFDocumentacion');
		$resolution = array(215.9,279.4);
		$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
		$pdf->SetTitle('ATENCION RECOMENDACIONES IID IE');
		$pdf->SetSubject('SIGFI');
		$pdf->SetKeywords('SIGFI');
		$pdf->SetPrintHeader(true);
		$pdf->SetPrintFooter(true);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->AddPage("P",$resolution);
		$pdf->writeHTML($body, true, false, true, false, '');
		$pdf->Output("TEST.pdf", 'D');
	}
	public function Ordenauditoriageneral($id="",$value='')
	{
		HeaderJson();
		$data["documento"]=$this->Infodocumentacion->GetOrdenAuditoriaGeneral($id);
		$body="";
		if ($data) {
			$body = $this->load->view('reportes/documentacion/ordenaduitoriageneral',$data,TRUE);
		}
		$this->load->library('PDFDocumentacion');
		$resolution = array(215.9,279.4);
		$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
		$pdf->SetTitle('ORDEN AUDITORIA GENERAL');
		$pdf->SetSubject('SIGFI');
		$pdf->SetKeywords('SIGFI');
		$pdf->SetPrintHeader(true);
		$pdf->SetPrintFooter(true);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->AddPage("P",$resolution);
		$pdf->writeHTML($body, true, false, true, false, '');
		$pdf->Output("TEST.pdf", 'D');
	}
	public function Atencionreciidiegeneral($id="",$value='')
	{
		HeaderJson();
		$data["documento"]=$this->Infodocumentacion->GetAtencionRecIIDIE($id);
		$body="";
		if ($data) {
			$body = $this->load->view('reportes/documentacion/atencionreciidiegeneral',$data,TRUE);
		}
		$this->load->library('PDFDocumentacion');
		$resolution = array(215.9,279.4);
		$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
		$pdf->SetTitle('ATENCION RECOMENDACIONES GENERAL');
		$pdf->SetSubject('SIGFI');
		$pdf->SetKeywords('SIGFI');
		$pdf->SetPrintHeader(true);
		$pdf->SetPrintFooter(true);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->AddPage("P",$resolution);
		$pdf->writeHTML($body, true, false, true, false, '');
		$pdf->Output("TEST.pdf", 'D');
	}
	public function Cuentapublicacpgeneral($id="",$value='')
	{
		HeaderJson();
		$data["documento"]=$this->Infodocumentacion->GetCuentaPublicaCPGeneral($id);
		$body="";
		if ($data) {
			$body = $this->load->view('reportes/documentacion/cuentapublicacpgeneral',$data,TRUE);
		}
		$this->load->library('PDFDocumentacion');
		$resolution = array(215.9,279.4);
		$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
		$pdf->SetTitle('CUENTA PUBLICA CP GENERAL');
		$pdf->SetSubject('SIGFI');
		$pdf->SetKeywords('SIGFI');
		$pdf->SetPrintHeader(true);
		$pdf->SetPrintFooter(true);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->AddPage("P",$resolution);
		$pdf->writeHTML($body, true, false, true, false, '');
		$pdf->Output("TEST.pdf", 'D');
	}
	public function Informeavancegfgeneral($id="",$value='')
	{
		HeaderJson();
		$data["documento"]=$this->Infodocumentacion->GetInformeAvanceGFGeneral($id);
		$body="";
		if ($data) {
			$body = $this->load->view('reportes/documentacion/informeavancegfgeneral',$data,TRUE);
		}
		$this->load->library('PDFDocumentacion');
		$resolution = array(215.9,279.4);
		$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
		$pdf->SetTitle('INFORME DE AVANCE GESTION FINANCIERA GENERAL');
		$pdf->SetSubject('SIGFI');
		$pdf->SetKeywords('SIGFI');
		$pdf->SetPrintHeader(true);
		$pdf->SetPrintFooter(true);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->AddPage("P",$resolution);
		$pdf->writeHTML($body, true, false, true, false, '');
		$pdf->Output("TEST.pdf", 'D');
	}
	public function Solicituddoccomgeneral($id="",$value='')
	{
		HeaderJson();
		$data["documento"]=$this->Infodocumentacion->GetSolicitudDocComGeneral($id);
		$body="";
		if ($data) {
			$body = $this->load->view('reportes/documentacion/solicituddoccomgeneral',$data,TRUE);
		}
		$this->load->library('PDFDocumentacion');
		$resolution = array(215.9,279.4);
		$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
		$pdf->SetTitle('SOLICITUD DOCUMENTACION COMPLEMENTARIA GENERAL');
		$pdf->SetSubject('SIGFI');
		$pdf->SetKeywords('SIGFI');
		$pdf->SetPrintHeader(true);
		$pdf->SetPrintFooter(true);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->AddPage("P",$resolution);
		$pdf->writeHTML($body, true, false, true, false, '');
		$pdf->Output("TEST.pdf", 'D');
	}
	public function solventacioniipeipgeneral($id="",$value='')
	{
		HeaderJson();
		$data["documento"]=$this->Infodocumentacion->GetSolventacionGeneral($id);
		$body="";
		if ($data) {
			$body = $this->load->view('reportes/documentacion/solventaciongeneral',$data,TRUE);
		}
		$this->load->library('PDFDocumentacion');
		$resolution = array(215.9,279.4);
		$pdf = new PDFDocumentacion('P', 'mm', $resolution, true, 'UTF-8', false);
		$pdf->SetTitle('ORDEN AUDITORIA GENERAL');
		$pdf->SetSubject('SIGFI');
		$pdf->SetKeywords('SIGFI');
		$pdf->SetPrintHeader(true);
		$pdf->SetPrintFooter(true);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->AddPage("P",$resolution);
		$pdf->writeHTML($body, true, false, true, false, '');
		$pdf->Output("TEST.pdf", 'D');
	}
}
