<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->helper(array('url', 'form','headers','jwt'));
    $this->load->model( array('Usuarios'));
  }
  public function index()
  {
    $data= array();
    $data["authorization"]=false;
    HeaderJson();
    header("HTTP/1.1 401 Unauthorized");
    echo json_encode($data);
  }
  public function Getoken($value='')
  {
    $data= array();
    if ($this->input->method()=="post" ) {
      $data["authorization"]=true;
      $data["apiversion"]=1;
      $data["date"]=date("Y-m-d H:i:s");
      $data["lista"]=$this->Usuarios->loginuser($this->input->post("loginuser"),base64_encode($this->input->post("contrasena")));
      if ($data["lista"]) {
        $data["token"]=JWT::encode($data["lista"],$this->config->item('jwt_key'));
      }
    }else{
      header("HTTP/1.1 401 Unauthorized");
      $data["authorization"]=false;
      $data["apiversion"]=1;
      $data["date"]=date("Y-m-d H:i:s");
    }
    HeaderJson();
    echo json_encode($data);
  }
}
