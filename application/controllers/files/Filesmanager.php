<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Filesmanager extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->helper(array('url', 'form','headers','jwt','auth','dates'));
    $this->load->model(array('sujetos/servidores/Expedientes'));
  }
  public function index()
  {
    HeaderJson();
    $arrayName["system"]="sigfi";
    echo json_encode($arrayName);
  }
  /**
  *
  *
  * @param null
  *
  * @return void
  */
  public function Expendiente($rfc="",$clave="")
  {
    $files = array();
    $path="./data/sujetos/".$clave."/servidores/".mb_strtoupper($rfc)."/expedientes";
    if (!file_exists($path)) {
      mkdir($path, 0777, true);
    }
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'doc|pdf|excel|docx';
    $config['max_size']      = '800000000';
    $this->load->library('upload', $config);
    $data=[];
    $insertfiles=[];
    $estadistic=[];
    $estadistic["files_upload"]=0;
    $estadistic["files_upload_error"]=0;
    for ($i=0; $i < $this->input->post("files"); $i++) {
      if (!$this->upload->do_upload('archivo'.$i))
      {
        $info["error"][] =$this->upload->display_errors();
        $estadistic["files_upload_error"]+=1;
      }
      else
      {
        $tempile=$this->upload->data();
        $info["file"][] =$this->upload->data();
        $insertfiles[]=array(
          'fk_servidor'=>mb_strtoupper($rfc),
          'archivo_url' => base_url().substr($path,2)."/".$tempile["file_name"],
          'fecha' => date("Y-m-d H:s:i"),
          'extension' => $tempile["file_ext"],
        );
        $estadistic["files_upload"]+=1;
      }
    }
    if(count($insertfiles)>0){
      for ($i=0; $i < count($insertfiles); $i++) {
        $this->Expedientes->Insert($insertfiles[$i]);
      }
    }
    $data["report"]=$estadistic;
    $data["info"]=$info;
    echo json_encode($data);
  }
}
