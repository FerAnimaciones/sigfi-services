<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Model for all CRUD for Expedientes.
*
* @author	FerAnimaciones
* @author	Fernando Manuel Avila Cataño
* @copyright	Copyright (c) 2018-2019, British Columbia Institute of Technology (http://bcit.ca/)
*/
class Servidores extends CI_Model {
  /** @var string|null Should contain db selected. */
  public	$db="default";

  public function InsertServidor($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->insert_batch('catservpublicosujetos', $data);
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }
  public function InsertServidorCambios($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->insert_batch('catservpublicosujetoscambios', $data);
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }
  public function UpdateServidor($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->update_batch('catservpublicosujetos', $data,'rfc');
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }
  public function InsertProveedorSuejto($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->insert_batch('proveedoressujeto', $data);
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }
  public function InsertPuestos($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->insert_batch('cat_puestos', $data);
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }

  public function InsertProfesion($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->insert_batch('cat_profesion', $data);
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }

  public function GetPuestos($id="")
 {
   $DBcon = $this->load->database($this->db, TRUE);
   $query=$DBcon->query("SELECT *  from cat_puestos as c  order by  c.idcat_puestos asc");
   if ($query->num_rows() > 0) {
     return $query->result();
   } else {
     return false;
   }
 }

 public function GetProfesion($id="")
 {
   $DBcon = $this->load->database($this->db, TRUE);
   $query=$DBcon->query("SELECT *  from cat_profesion as c    order by  c.idcat_profesion asc");
   if ($query->num_rows() > 0) {
     return $query->result();
   } else {
     return false;
   }
 }

 public function GetServidoresPublicosEnte($idente=0)
 {
   $DBcon = $this->load->database($this->db, TRUE);
   $query=$DBcon->query("SELECT * from catservpublicosujetos as c    where c.fk_sujeto=$idente ");
   if ($query->num_rows() > 0) {
     return $query->result();
   } else {
     return false;
   }
 }

 public function GetProveedoresSujeto($idente)
 {
   $DBcon = $this->load->database($this->db, TRUE);
   $query=$DBcon->query("SELECT * from proveedoressujeto as c where c.fk_sujeto=$idente ");
   if ($query->num_rows() > 0) {
     return $query->result();
   } else {
     return false;
   }
 }
 public function GetProveedores($idente=0)
 {
   $DBcon = $this->load->database($this->db, TRUE);
   $query=$DBcon->query("SELECT rfc from proveedores ");
   if ($query->num_rows() > 0) {
     return $query->result();
   } else {
     return false;
   }
 }
}
