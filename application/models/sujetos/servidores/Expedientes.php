<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Model for all CRUD for Expedientes.
*
* @author	FerAnimaciones
* @author	Fernando Manuel Avila Cataño
* @copyright	Copyright (c) 2018-2019, British Columbia Institute of Technology (http://bcit.ca/)
*/
class Expedientes extends CI_Model {
  /** @var string|null Should contain db selected. */
  public	$db="default";

  public function Insert($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->insert('expedientedigital', $data);
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }
}
