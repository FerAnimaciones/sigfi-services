<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Model for all CRUD for Expedientes.
*
* @author	FerAnimaciones
* @author	Fernando Manuel Avila Cataño
* @copyright	Copyright (c) 2018-2019, British Columbia Institute of Technology (http://bcit.ca/)
*/
class Fiscalizables extends CI_Model {
  /** @var string|null Should contain db selected. */
  public	$db="default";
  public function InsertSujetoFiscalizable($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->insert_batch('sujetofiscalizable', $data);
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }

  public function UpdateSujetoFiscalizable($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->update_batch('sujetofiscalizable', $data,'clave');
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }

  public function InsertCatTipoEntes($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->insert_batch('cat_tipo_entes', $data);
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }


  public function GetSujetosFiscalizables($id="")
 {
   $DBcon = $this->load->database($this->db, TRUE);
   $query=$DBcon->query("SELECT *  from sujetofiscalizable as sj  order by  sj.clave asc");
   if ($query->num_rows() > 0) {
     return $query->result();
   } else {
     return false;
   }
 }

 public function GetCatTipoEntes($id="")
 {
   $DBcon = $this->load->database($this->db, TRUE);
   $query=$DBcon->query("SELECT *  from cat_tipo_entes as c  order by  c.idcat_tipo_entes asc");
   if ($query->num_rows() > 0) {
     return $query->result();
   } else {
     return false;
   }
 }

 public function GetEstado($id="")
 {
   $DBcon = $this->load->database($this->db, TRUE);
   $query=$DBcon->query("SELECT * from estado as e
     order by e.nombre asc ");
   if ($query->num_rows() > 0) {
     return $query->result();
   } else {
     return false;
   }
 }

 public function GetMunicipio($id="")
 {
   $DBcon = $this->load->database($this->db, TRUE);
   $query=$DBcon->query("SELECT * from municipioDTO as m
     order by m.siglas_mun asc ");
   if ($query->num_rows() > 0) {
     return $query->result();
   } else {
     return false;
   }
 }

 public function GetLocalidad($id="")
 {
   $DBcon = $this->load->database($this->db, TRUE);
   $query=$DBcon->query("SELECT rfc from localidadDTO as l");
   if ($query->num_rows() > 0) {
     return $query->result();
   } else {
     return false;
   }
 }
 public function GetClasesTipoEnte($id="")
 {
   $DBcon = $this->load->database($this->db, TRUE);
   $query=$DBcon->query("select * from tipoclase as tc,clasetipoente as ct where fkclase=idclasetipoente");
   if ($query->num_rows() > 0) {
     return $query->result();
   } else {
     return false;
   }
 }
}
