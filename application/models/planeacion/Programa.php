<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Model for all CRUD for Planeacion
*
* @author	FerAnimaciones
* @author	Fernando Manuel Avila Cataño
* @copyright	Copyright (c) 2018-2019, British Columbia Institute of Technology (http://bcit.ca/)
*/
class Programa extends CI_Model {
  /** @var string|null Should contain db selected. */
  public	$db="default";
  public function GetTodayProgramaTipoDateProgram($id="")
  {
    $today=date('Y-m-d');
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT pat.*,ctp.nombre from programaauditoriatipo as pat,cat_tipo_auditoria as ctp
      WHERE fhnotificacionesfechas >= '$today 00:00:00'::timestamp AND  fhnotificacionesfechas < '$today 23:59:59' AND fechas=false and  pat.cat_tipo_auditoria_idcat_tipo_auditoria=ctp.idcat_tipo_auditoria and pat.programaauditoria_idprogramaauditoria=$id"
    );
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetTodayProgramaAuditoria($id="")
  {
    $today=date('Y-m-d');
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT DISTINCT (p.idprogramaauditoria),s.nombre_ente,p.* from programaauditoriatipo as pat,programaauditoria as p,sujetofiscalizable as s
    WHERE pat.fhnotificacionesfechas >= '$today 00:00:00' AND  pat.fhnotificacionesfechas < '$today 23:59:59' AND pat.fechas=false and pat.programaauditoria_idprogramaauditoria=p.idprogramaauditoria and p.clave=s.clave"  );
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetUpdatePgAT($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->update_batch('programaauditoriatipo', $data,'idprogramaauditoriatipo');
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }

  public function GetConsultaTipoNumAudi($aafiscal="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("select ca.nombre,count(pgu.idprogramaauditoriatipo) as totalindividual from programaauditoriatipo as pgu,cat_tipo_auditoria as ca,programaauditoria as pg".
    " where cat_tipo_auditoria_idcat_tipo_auditoria=ca.idcat_tipo_auditoria and pg.idprogramaauditoria=programaauditoria_idprogramaauditoria ".
    " and pg.aafiscal=$aafiscal group by ca.idcat_tipo_auditoria order by ca.nombre desc");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }

  public function GetSujetosByTipoEnte($a="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("select ct.* from cat_tipo_entes as ct order by nombre desc;");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }

  public function GetSujetosByTipoEnteEx($aafiscal="",$clase="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("select ct.*,count(ct.nombre) as sujetotipo from 	programaauditoria as pg,sujetofiscalizable as sj,cat_tipo_entes as ct where
    pg.aafiscal=$aafiscal and pg.fk_clave=sj.clave and sj.cat_tipo_entes_idcat_tipo_entes=ct.idcat_tipo_entes and ct.tipofiscalizador='$clase'
    group by ct.idcat_tipo_entes");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetSujetosByTipoEnteAll($aafiscal="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("select ct.*,count(ct.nombre) as sujetotipo from 	programaauditoria as pg,sujetofiscalizable as sj,cat_tipo_entes as ct where
    pg.aafiscal=$aafiscal and pg.fk_clave=sj.clave and sj.cat_tipo_entes_idcat_tipo_entes=ct.idcat_tipo_entes   group by ct.idcat_tipo_entes");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetAuditoriasTipoEnte($aafiscal="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("select ct.*,count(ct.idcat_tipo_entes) AS total_auditorias from programaauditoriatipo as pgu,programaauditoria as pg,sujetofiscalizable as sj,cat_tipo_entes as ct,cat_tipo_auditoria as ca where
    pg.aafiscal=$aafiscal and pg.fk_clave=sj.clave and sj.cat_tipo_entes_idcat_tipo_entes=ct.idcat_tipo_entes and
    pgu.cat_tipo_auditoria_idcat_tipo_auditoria=ca.idcat_tipo_auditoria  and pg.idprogramaauditoria=programaauditoria_idprogramaauditoria
    group by ct.idcat_tipo_entes");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }

  public function GetTipoAuditorias($a="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("select  *  from cat_tipo_auditoria where idcat_tipo_auditoria!=1 ");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }

  public function GetSujetoAuditoriasbyclase($aafiscal="",$clase="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("select  *  from programaauditoriatipo as pgu,programaauditoria as pg,sujetofiscalizable as sj,cat_tipo_entes as ct,cat_tipo_auditoria as ca where
    pg.aafiscal=$aafiscal and pg.fk_clave=sj.clave and sj.cat_tipo_entes_idcat_tipo_entes=ct.idcat_tipo_entes and ct.idcat_tipo_entes=$clase and
    pgu.cat_tipo_auditoria_idcat_tipo_auditoria=ca.idcat_tipo_auditoria  and pg.idprogramaauditoria=programaauditoria_idprogramaauditoria order by sj.nombre_ente asc");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }

  public function GetSujetoAuditoriasbyclasetipoaudia($aafiscal="",$clase="",$tipoaudi=0)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("select  *,cf.descripcion as crisel from programaauditoriatipo as pgu,criterios_fiscalizar as cf,programaauditoria as pg,sujetofiscalizable as sj,cat_tipo_entes as ct,cat_tipo_auditoria as ca where
    pg.aafiscal=$aafiscal and pg.fk_clave=sj.clave and sj.cat_tipo_entes_idcat_tipo_entes=ct.idcat_tipo_entes and ct.idcat_tipo_entes=$clase and
    pgu.cat_tipo_auditoria_idcat_tipo_auditoria=ca.idcat_tipo_auditoria  and pg.idprogramaauditoria=programaauditoria_idprogramaauditoria and   pgu.cat_tipo_auditoria_idcat_tipo_auditoria=$tipoaudi and cf.idcriterios_fiscalizar=pgu.criterios_fiscalizar_idcriterios_fiscalizar order by sj.nombre_ente asc");
    if ($query->num_rows() > 0) {
      //echo $DBcon->last_query();
      return $query->result();
    } else {
      return false;
    }
  }

  public function GetPersonalRespSuperv($a="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("select  catente.nombre, catente.apellido_pa, catente.apellido_ma, catp.nombre as puesto, catente.iniciales from
    cat_servpublicosente as catente, cat_puestos as catp, programaauditoriatipo as pat,
    auditoriaprocedimientos as ap,personalauditor as pau where
    pat.programaauditoria_idprogramaauditoria = $a and pat.idprogramaauditoriatipo = ap.fk_programatipo and
    ap.fk_supervisor = pau.idpersonalauditor
    and catp.idcat_puestos = catente.cat_puestos_idcat_puestos and
    catente.idcat_servpublicos = pau.cat_servpublicosente_idcat_servpublicos");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetPersonalAuditor($a="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("select catente.nombre, catente.apellido_pa, catente.apellido_ma, catp.nombre as puesto, catente.iniciales from
    cat_servpublicosente as catente, cat_puestos as catp, programaauditoriatipo as pat,personalauditor as pau where
    pat.programaauditoria_idprogramaauditoria = $a and pat.idprogramaauditoriatipo = pau.programaauditoriatipo_idprogramaauditoriatipo
    and catp.idcat_puestos = catente.cat_puestos_idcat_puestos and
    catente.idcat_servpublicos = pau.cat_servpublicosente_idcat_servpublicos");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
}
