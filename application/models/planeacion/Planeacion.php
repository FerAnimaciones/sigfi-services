<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Model for all CRUD for Planeacion
*
* @author	FerAnimaciones
* @author	Fernando Manuel Avila Cataño
* @copyright	Copyright (c) 2018-2019, British Columbia Institute of Technology (http://bcit.ca/)
*/
class Planeacion extends CI_Model {
  /** @var string|null Should contain db selected. */
  public	$db="default";
  public function GetProgramaAuditoria($id="",$id2="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT pa.*,pat.*,cm.*,  cta.nombre as auditoria,obj.descripcion as objetivo, sj.*,e.desentidad,cf.descripcion as cfdes,
      cf.clave as cfclave,mj.clave as modalidadejec,mj.descripcion as modalidadejecdes from programaauditoria as pa,sujetofiscalizable as sj,entidad as e,criterios_fiscalizar as cf,
      programaauditoriatipo as pat, criterios_muestra as cm, objetivos as obj, cat_tipo_auditoria as cta, modalidad_ejec as mj
      where pa.idprogramaauditoria=$id and pat.idprogramaauditoriatipo=$id2
      and pa.fk_clave=sj.clave and e.cveentidad=sj.estado and
      cf.idcriterios_fiscalizar=pat.criterios_fiscalizar_idcriterios_fiscalizar and cm.idcriterios_muestra=pat.criterios_muestra_idcriterios_muestra
      and cta.idcat_tipo_auditoria=pat.cat_tipo_auditoria_idcat_tipo_auditoria and obj.idobjetivos=pat.objetivos_idobjetivos and pa.modalidad_ejec_idmodalidad_ejec=mj.idmodalidad_ejec"
    );
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetProgramaAuditoriaTipoPersonal($id="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT csp.apellido_ma,csp.apellido_pa, csp.nombre, cc.nombre as cargo
      from personalauditor as pa,cat_cargos as cc, cat_servpublicosente as csp
      where pa.programaauditoriatipo_idprogramaauditoriatipo =$id
      and csp.cat_cargos_idcat_cargos=cc.idcat_cargos and
      csp.idcat_servpublicos=pa.cat_servpublicosente_idcat_servpublicos order by  cc.prioridad asc"
    );
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetDirectorUnidadTipoAuditoria($id="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT cpe.apellido_ma,cpe.apellido_pa, cpe.nombre, ud.nombre as Unidad, pu.nombre as puesto, cc. nombre as cargo
      from cat_servpublicosente as cpe ,unidadadministrativa as ud, direccionadministrativa as dir, departamentoadministrativo as da, cat_cargos as cc, cat_puestos as pu
      where cpe.departamentoadministrativo_iddepartamentoadministrativo=da.iddepartamentoadministrativo
      and da.direccionadministrativa_iddireccionadministrativa=dir.iddireccionadministrativa and
      dir.unidadadministrativa_iduad=ud.iduad and ud.cat_tipo_auditoria_idcat_tipo_auditoria=$id and cpe.cat_cargos_idcat_cargos =2
	  and cpe.cat_cargos_idcat_cargos=cc.idcat_cargos  and cpe.cat_puestos_idcat_puestos=pu.idcat_puestos "
    );
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetUnidadAdministrativa($id="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT ud.nombre
      from unidadadministrativa as ud
      where ud.cat_tipo_auditoria_idcat_tipo_auditoria=$id "
    );
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetProcedimientosAuditoria($id="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT ap.*,cspe.iniciales from auditoriaprocedimientos as ap,cat_servpublicosente as cspe where ap.fk_supervisor= cspe.idcat_servpublicos and fk_programatipo=$id" );
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetActividadesProcedimientosAuditoria($id="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT ap.*,cspe.iniciales from procedimientosactividades as ap,cat_servpublicosente as cspe where ap.fk_supervisor= cspe.idcat_servpublicos and fk_audiproc=$id order by ap.orden asc" );
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetProcedimientosAuditoriaPersonal($id="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT cspe.* from personalelaboraproc as per, cat_servpublicosente as cspe where per.fk_personal= cspe.idcat_servpublicos and per.fk_audiproc=$id" );
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetSupervisoresProcedimiento($id="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT cspe.* from auditoriaprocedimientos as ap, cat_servpublicosente as cspe
      where ap.fk_supervisor= cspe.idcat_servpublicos and fk_programatipo=$id"   );
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetSupervisorActividades($id="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT cspe.*  from cat_servpublicosente as cspe, procedimientosactividades as pa   where pa.idactividad=$id and pa.fk_supervisor=cspe.idcat_servpublicos"    );
    if ($query->num_rows() > 0) {
      return $query->row();
    } else {
      return false;
    }
  }
  public function GetPersonalActividad($id="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT cspe.*  from cat_servpublicosente as cspe, personalactividad as pa   where pa.fk_actividad=$id and pa.fk_personal=cspe.idcat_servpublicos"    );
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
}
