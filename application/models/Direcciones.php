<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Model for all CRUD for Direcciones
*
* @author	FerAnimaciones
* @author	Fernando Manuel Avila Cataño
* @copyright	Copyright (c) 2018-2019, British Columbia Institute of Technology (http://bcit.ca/)
*/
class Direcciones extends CI_Model {
  /** @var string|null Should contain db selected. in this cases SQLlite */
  public	$db="default";
  public function Getdirbyid($id=0)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from direccionadministrativa where iddireccionadministrativa=$id  ");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
}
