<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Infodocumentacion extends CI_Model {
  public $db="default";
  public function __construct() {
    parent::__construct();
  }
  public function GetOrdenAuditoria($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from ordenauditoria as so,sujetofiscalizable as su,cuenta_publica as cp,cat_tipo_entes te where so.idordenauditoria=$id and so.sujetofiscalizable_idsujetofiscalizable=su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes and cp.cat_tipo_entes_idcat_tipo_entes=te.idcat_tipo_entes and so.cuenta_publica_idcuenta_publica=cp.idcuenta_publica");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {  
      return false;
    }
  }
  public function GetSolventacion($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from solventacioniipeip as so,sujetofiscalizable as su, cat_tipo_entes te where so.idsolventacioniipeip=$id and so.sujetofiscalizable_idsujetofiscalizable=su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes ");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetSolicitudDocCom($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from solicituddoccom as so,sujetofiscalizable as su,cuenta_publica as cp,cat_tipo_entes te where so.solicituddoccom=$id and so.sujetofiscalizable_idsujetofiscalizable=su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes and cp.cat_tipo_entes_idcat_tipo_entes=te.idcat_tipo_entes and so.cuenta_publica_idcuenta_publica=cp.idcuenta_publica");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetCuentaPublicaCP($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from cuentapublicacp as so,sujetofiscalizable as su,cuenta_publica as cp,cat_tipo_entes te where so.idcuentapublicacp=$id and so.sujetofiscalizable_idsujetofiscalizable=su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes and cp.cat_tipo_entes_idcat_tipo_entes=te.idcat_tipo_entes and so.cuenta_publica_idcuenta_publica=cp.idcuenta_publica");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetInformeAvanceGF($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from informeavancegf as so,sujetofiscalizable as su, cat_tipo_entes te where so.idinformeavancegf=$id and so.sujetofiscalizable_idsujetofiscalizable=su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes ");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetAtencionRecIIDIE($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from atencionreciidie as so,sujetofiscalizable as su, cat_tipo_entes te where so.idatencionreciidie=$id and so.sujetofiscalizable_idsujetofiscalizable=su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes ");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetOrdenAuditoriaGeneral($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from ordenauditoria as so,sujetofiscalizable as su,cuenta_publica as cp,cat_tipo_entes te where cp.idcuenta_publica=so.cuenta_publica_idcuenta_publica and so.sujetofiscalizable_idsujetofiscalizable= su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes and cp.cat_tipo_entes_idcat_tipo_entes=te.idcat_tipo_entes and so.cuenta_publica_idcuenta_publica=$id");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetAtencionRecIIDIEgeneral($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from ordenauditoria as so,sujetofiscalizable as su,cuenta_publica as cp,cat_tipo_entes te where cp.idcuenta_publica=so.cuenta_publica_idcuenta_publica and so.sujetofiscalizable_idsujetofiscalizable= su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes and cp.cat_tipo_entes_idcat_tipo_entes=te.idcat_tipo_entes and so.cuenta_publica_idcuenta_publica=$id");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetCuentaPublicaCPGeneral($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from ordenauditoria as so,sujetofiscalizable as su,cuenta_publica as cp,cat_tipo_entes te where cp.idcuenta_publica=so.cuenta_publica_idcuenta_publica and so.sujetofiscalizable_idsujetofiscalizable= su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes and cp.cat_tipo_entes_idcat_tipo_entes=te.idcat_tipo_entes and so.cuenta_publica_idcuenta_publica=$id");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetInformeAvanceGFGeneral($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from ordenauditoria as so,sujetofiscalizable as su,cuenta_publica as cp,cat_tipo_entes te where cp.idcuenta_publica=so.cuenta_publica_idcuenta_publica and so.sujetofiscalizable_idsujetofiscalizable= su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes and cp.cat_tipo_entes_idcat_tipo_entes=te.idcat_tipo_entes and so.cuenta_publica_idcuenta_publica=$id");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetSolicitudDocComGeneral($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from ordenauditoria as so,sujetofiscalizable as su,cuenta_publica as cp,cat_tipo_entes te where cp.idcuenta_publica=so.cuenta_publica_idcuenta_publica and so.sujetofiscalizable_idsujetofiscalizable= su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes and cp.cat_tipo_entes_idcat_tipo_entes=te.idcat_tipo_entes and so.cuenta_publica_idcuenta_publica=$id");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetSolventacionGeneral($id)
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from ordenauditoria as so,sujetofiscalizable as su,cuenta_publica as cp,cat_tipo_entes te where cp.idcuenta_publica=so.cuenta_publica_idcuenta_publica and so.sujetofiscalizable_idsujetofiscalizable= su.idsujetofiscalizable and te.idcat_tipo_entes= su.cat_tipo_entes_idcat_tipo_entes and cp.cat_tipo_entes_idcat_tipo_entes=te.idcat_tipo_entes and so.cuenta_publica_idcuenta_publica=$id");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
}
