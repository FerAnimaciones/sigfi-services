<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Correos extends CI_Model {
  //public $basesystem="http://localhost/sigfi/";
  public $basesystem="http://201.107.4.145/";
  function __construct() {
    parent::__construct();
    $this->load->helper(array('url', 'form','headers'));
    $this->load->model(array('Systeminfo'));
  }
  public function EnviarCorreo($data,$correos)
  {
    $data["base_system"]=$this->basesystem;
    $data["systeminfo"]=$this->Systeminfo->GetInfoSystema();
    $reponse=false;
    if (true) {
      try {
        $this->load->library("email");
        $configGmail = array(
          'protocol' => 'smtp',
          'smtp_host' => 'tls://smtp.gmail.com',
          'smtp_host' => 'smtp.gmail.com',
          'smtp_crypto' => 'tls',
          'smtp_port' => 587,
          'smtp_user' => 'feranimacionestestng1@gmail.com',
          'smtp_pass' => '%Carolina__21%',
          'mailtype' => 'html',
          'charset' => 'utf-8',
          'newline' => "\r\n"
        );
        $this->email->initialize($configGmail);
        $this->email->from('feranimacionestestng1@gmail.com', 'Sistema de Información para la Gestión de la Fiscalización Pública');
        $this->email->to(implode(', ', $correos));
        $this->email->cc('feranimacionestestng1@gmail.com');
        $this->email->subject($data["titulo"]);
        $emailbody = $this->load->view('email/email.php',$data,TRUE);
        $this->email->message($emailbody);
        //echo $emailbody;
        //$reponse["send"]=true;
        if ($this->email->send(FALSE)) {
          $reponse["send"]=true;
        } else {
          $reponse["send"]=false;
        }

      } catch (\Exception $e) {
        $reponse["send"]=false;
        $reponse["error_message"]=$e;
      }
    }
    return $reponse;
  }
}
