<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Systeminfo extends CI_Model {
  public $db="default";
  public function __construct() {
    parent::__construct();
  }
  public function GetInfoSystema($id="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from systeminfo where idsystem=1");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
}
