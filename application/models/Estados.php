<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Model for all CRUD for Estados
*
* @author	FerAnimaciones
* @author	Fernando Manuel Avila Cataño
* @copyright	Copyright (c) 2018-2019, British Columbia Institute of Technology (http://bcit.ca/)
*/
class Estados extends CI_Model {
  /** @var string|null Should contain db selected. in this cases SQLlite */
  public	$db="default";
  public function GetEntidades($s="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from entidad ");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetMunicipios($cveentidad="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from municipio where cveentidad=$cveentidad ");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function GetLocalidades($cveentidad="",$cvemunicipio="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT * from localidad where cveentidad=$cveentidad and cvemunicipio=$cvemunicipio and  cvelocalidad!=0 ");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
}
