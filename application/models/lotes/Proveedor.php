<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Model for all CRUD for Planeacion
*
* @author	FerAnimaciones
* @author	Fernando Manuel Avila Cataño
* @copyright	Copyright (c) 2018-2019, British Columbia Institute of Technology (http://bcit.ca/)
*/
class Proveedor extends CI_Model {
  /** @var string|null Should contain db selected. */
  public	$db="default";
  public function GetProvedor($id="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $query=$DBcon->query("SELECT rfc from proveedores");
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function UpdateProvedor($id="",$data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->where('rfc',$id);
    $DBcon->update('proveedores', $data);
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }
  public function InsertProvedor($data="")
  {
    $DBcon = $this->load->database($this->db, TRUE);
    $DBcon->insert('proveedores', $data);
    if($DBcon->affected_rows()>0){
      return true;
    }else{
      return false;
    }
  }
}
