<h2>Falta la asiganci&oacute;n de fechas del Programa de Auditor&iacute;a</h2>
<p>N&uacute;mero de Auditor&iacute;a:<b> <?php  if (isset($programamaactual)) { echo $programamaactual->numeroauditoria;} ?></b></p>
<p>Sujeto Fiscalizable:<b> <?php  if (isset($programamaactual)) { echo $programamaactual->nombre_ente;} ?></b></p>

<p>Fecha de elaboraci&oacute;n:<b> <?php  if (isset($programamaactual)) { echo date("d-m-Y H:i:s", strtotime($programamaactual->fhelaboracion));} ?></b></p>

<br><br>
<p>Fecha para la siguiente notificaci&oacute;n</p>
<p><b><?php  if (isset($nextnotification)) {echo date("d-m-Y", strtotime($nextnotification));} ?></b></p>
