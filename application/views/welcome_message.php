<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>LEVOCON SERVICES API</title>
  <meta http-equiv="X-UA-Compatible" content="IE=9,10,11,edge" />
  <link rel="manifest" href="<?= base_url(); ?>manifest.json">
  <link rel="apple-touch-icon" sizes="57x57" href="images/icons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="images/icons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="images/icons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="images/icons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="images/icons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="images/icons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="images/icons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="images/icons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="images/icons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="images/icons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png">
  <link href='mages/icons/favicon.ico' rel='icon' type='image/x-icon'/>
  <style media="screen">
    @import url(https://fonts.googleapis.com/css?family=Lato:300,400,700);
    body {
      background: #434A54;
      padding: 0;
      margin: 0;
      border: 0;
    }
    .container {
      margin: auto;
      width: 37%;
      margin-top: calc(100vh - 70vh);
    }
    .logo {
      font-weight: 400;
      font-family: 'Lato', Calibri, Arial, sans-serif;
      color: #E6E9ED;
      font-size: 80px;
      position: relative;
      margin: 0;
    }

    .word {
      color: #3BAFDA;
    }

    .right, .left {
      font-family: arial;
      font-size: 180px;
      position: relative;
      top: 55px;
      color: #3BAFDA;
      margin: 0 0 0 20px;
    }

    .text {
      font-size: 30px;
      position: absolute;
      top: 168px;
      left: 214px;
    }
    .left {
      margin: 0 30px 0 0;
    }

  </style>
</head>
<body>

  <div class="container">
    <div class="logo">
      <span class="left"><</span>LEVOCON<span class="right">/></span>
      <div class="text">
        <span class="word">API</span>  <span class="web">REST</span>
      </div>
    </div>
  </div>
</body>
</html>
