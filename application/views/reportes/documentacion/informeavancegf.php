<!DOCTYPE html>
<html>
<head>
</head>
<body>
  <?php
  if ($documento) {
    //var_dump($documento);
    foreach ($documento as $key => $value) {
      ?>
      <table style="border-collapse: collapse; width: 100%;" border="0">
        <tbody>
          <tr>
            <td style="width: 33.3333%;"><b>AÑO:</b>  </td>
            <td style="width: 33.3333%;">&nbsp;</td>
            <td style="width: 33.3333%;"><b>TRIMESTRE:</b> </td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><br></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><b>ENTE FIZCALIZADOR:</b></td>
            <td style="width: 33.3333%;"><?= $value->nombre_ente; ?></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><br></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><b>TIPO ENTE:</b></td>
            <td style="width: 33.3333%;"><?= $value->nombre; ?></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><br></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><b>FECHA LÍMITE PRESENTACIÓN:</b></td>
            <td style="width: 33.3333%;">&nbsp;</td>
            <td style="width: 33.3333%;"><?= substr($value->fhlimpresentacion,0,10);  ?></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><b>EXTEMPORANEIDAD: </b></td>
            <td style="width: 33.3333%;">&nbsp;</td>
            <td style="width: 33.3333%;"><?= $value->dextempo; ?></td>

          </tr>
          <tr>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><b>PRÓRROGA:</b></td>
            <td style="width: 33.3333%;">&nbsp;</td>
            <td style="width: 33.3333%;"><?= $value->prorroga; ?></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>

          <?php
            if ($value->prorroga=="SI") {
             ?>
             <tr>
               <td style="width: 33.3333%;"><b>DÍAS AUTORIZADOS DE PRÓRROGA:</b></td>
               <td style="width: 33.3333%;"><b>FECHA VENCIMIENTO PRÓRROGA</b></td>
             </tr>
             <tr style="line-height: 500%;">
               <td style="width: 33.3333%;"><?= $value->dautorizadosprorro;  ?></td>
               <td style="width: 33.3333%;"><?= substr($value->fhvencrorroga,0,10);  ?></td>
             </tr>
             <?php
            }
           ?>
        </tbody>
      </table>
      <?php
    }
  }

  ?>

</body>
</html>
