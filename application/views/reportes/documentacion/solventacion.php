<!DOCTYPE html>
<html>
<head>
</head>
<body>
  <?php
  if ($documento) {
    //var_dump($documento);
    foreach ($documento as $key => $value) {
      ?>
      <table style="border-collapse: collapse; width: 100%;" border="0">
        <tbody>
          <tr>
            <td style="width: 33.3333%;"><b>NO. OFICIO:</b> <?= $value->numoficionoti; ?></td>
            <td style="width: 33.3333%;">&nbsp;</td>
            <td style="width: 33.3333%;"><b>ELABORACIÓN:</b><?= substr($value->fhelaboracion,0,10);  ?></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><br></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><b>ENTE FIZCALIZADOR:</b></td>
            <td style="width: 33.3333%;"><?= $value->nombre_ente; ?></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><br></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><b>TIPO ENTE:</b></td>
            <td style="width: 33.3333%;"><?= $value->nombre; ?></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>

          <tr>
            <td style="width: 33.3333%;"><b>EXTEMPORANEIDAD: </b></td>
            <td style="width: 33.3333%;">&nbsp;</td>
            <td style="width: 33.3333%;"><?= $value->diasextempo; ?></td>

          </tr>
          <tr>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><b>FECHA PRESENTACIÓN: </b></td>
            <td style="width: 33.3333%;">&nbsp;</td>
            <td style="width: 33.3333%;"><?= substr($value->fhpresentacion,0,10); ?></td>

          </tr>
          <tr>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
            <td style="width: 33.3333%;"></td>
          </tr>
          <tr>
            <td style="width: 33.3333%;"><b>FECHA DE NOTIFICACIÓN:</b></td>
            <td style="width: 33.3333%;"><b>DÍAS OTORGADOS:</b></td>
            <td style="width: 33.3333%;"><b>FECHA LÍMITE DE PRESENTACIÓN:</b></td>
          </tr>

             <tr style="line-height: 500%;">
               <td style="width: 33.3333%;"><?= substr($value->fhnotificacion,0,10);  ?></td>
               <td style="width: 33.3333%;"><?= $value->numdiasotor; ?></td>
               <td style="width: 33.3333%;"><?= substr($value->fhlimite,0,10);  ?></td>
             </tr>

        </tbody>
      </table>
      <?php
    }
  }

  ?>

</body>
</html>
