<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'>
<head><title>Programa de Auditoria</title>
  <link rel=File-List href="mydocument_files/filelist.xml">
  <style>
  @page
  {
    size:21cm 29.7cmt;  /* A4 */
    margin:1cm 1cm 1cm 2cm; /* Margins: 2.5 cm on each side */
    mso-page-orientation: portrait;
  }
  @page Section1 { }
  div.Section1 { page:Section1; }
  p.MsoHeader, p.MsoFooter { border: 1px solid black; }
  .tabla{
    width: 100%;
  }
</style>
</head>
<body>
  <div class=Section1>
    <table cellpadding="7" cellspacing="0" class="tabla">
      <tbody>
        <tr>
          <td colspan="1" width="20%">
            <img src="<?php echo base_url(); ?>formatos/test/test.png" width="150" height="70">
          </td>
          <td colspan="5" width="70%">
            <center><b>AUDITORÍA SUPERIOR DEL ESTADO DE NAYARIT
              PROGRAMA ESPECÍFICO DE AUDITORÍA (GENERAL)</b></center>
            </td>
          </tr>
          <tr>
            <td colspan="4" width="64.93738819320215%">
              <p>NOMBRE DEL ENTE</p>
              <p>
                <p><?= $datos->sujeto->nombre_ente ?></p>
              </p>
            </td>
            <td colspan="2" width="35.062611806797854%">
              <p>No. AUDITOR&Iacute;A&nbsp;</p>
              <p><?= $datos->numeroauditoria ?></p>
            </td>
          </tr>
          <tr>
            <td colspan="4" width="64.93738819320215%">
              <p>TIPO DE FISCALIZACI&Oacute;N</p>
              <p>
                <?php
                if ($datos->tpfiscal=="C") {
                  echo "Cuenta Publica";
                }
                if ($datos->tpfiscal=="S") {
                  echo "Auditoria Especial";
                }
                ?>
              </p>
            </td>
            <td colspan="2" width="35.062611806797854%">
              <p>EJERCICIO FISCAL&nbsp;</p>
              <p><?= $datos->aafiscal ?></p>
            </td>
          </tr>
          <tr>
            <td width="31.926605504587155%">
              <p>TIPO DE AUDITOR&Iacute;A</p>
            </td>
            <td colspan="2" width="32.11009174311926%">
              <p>DIRECCI&Oacute;N RESPONSABLE</p>
            </td>
            <td colspan="3" width="35.96330275229358%">
              <p>DEPARTAMENTO RESPONSABLE</p>
            </td>
          </tr>
          <tr>
            <td width="31.926605504587155%">
              <p>${tipoauditoria}</p>
            </td>
            <td colspan="2" width="32.11009174311926%">

              <p>${tipoaudir}</p>
            </td>
            <td colspan="3" width="35.96330275229358%">

              <p>${tipoaudepa}</p>
            </td>
          </tr>
          <tr>
            <td colspan="6" width="100%">

              <p><strong>PERSONAL AUDITOR</strong></p>
            </td>
          </tr>
          <tr>
            <td height="10" colspan="2" width="47.61029411764706%">
              NOMBRE
            </td>
            <td height="10" colspan="3" width="37.13235294117647%">
              PUESTO
            </td>
            <td height="10" width="15.257352941176471%">
              INICIALES
            </td>
          </tr>
          <?php

          foreach ($auditores as $key => $value) {
            ?>
            <tr>
              <td height="10" colspan="2" width="47.61029411764706%">
                <?= $value["nombre"] ?>
              </td>
              <td height="10" colspan="3" valign="top" width="37.13235294117647%" >
                <?= $value["puesto"] ?>
              </td>
              <td height="10" valign="top" width="15.257352941176471%">
                <?= $value["iniciales"] ?>
              </td>
            </tr>
            <?php
          }

          ?>

          <tr>
            <td colspan="6" width="100%">

              <p><strong>PERSONAL RESPONSABLE DE LA SUPERVISI&Oacute;N</strong></p>
            </td>
          </tr>
          <tr>
            <td colspan="2" width="47.61029411764706%">

              <p>NOMBRE</p>
            </td>
            <td colspan="3" width="37.13235294117647%">

              <p>PUESTO</p>
            </td>
            <td width="15.257352941176471%">

              <p>INICIALES</p>
            </td>
          </tr>
          <tr>
            <td colspan="2" width="47.61029411764706%">

              <p>Aid&eacute; Herrera Santana</p>
            </td>
            <td colspan="3" width="37.13235294117647%">

              <p>Directora AF</p>
            </td>
            <td width="15.257352941176471%">

              <p>AHS</p>
            </td>
          </tr>
          <tr>
            <td colspan="2" width="47.61029411764706%">

              <p>Carmen Beatriz P&eacute;rez Rodr&iacute;guez</p>
            </td>
            <td colspan="3" width="37.13235294117647%">

              <p>Jefe de Departamento AF</p>
            </td>
            <td width="15.257352941176471%">

              <p>CBPR</p>
            </td>
          </tr>
          <tr>
            <td colspan="2" width="47.61029411764706%">

              <p>
                <br>
              </p>
            </td>
            <td colspan="3" width="37.13235294117647%">

              <p>Director AOP</p>
            </td>
            <td width="15.257352941176471%">

              <p>
                <br>
              </p>
            </td>
          </tr>
          <tr>
            <td colspan="2" width="47.61029411764706%">

              <p>
                <br>
              </p>
            </td>
            <td colspan="3" width="37.13235294117647%">

              <p>Jefe de Departamento AOP</p>
            </td>
            <td width="15.257352941176471%">

              <p>
                <br>
              </p>
            </td>
          </tr>
          <tr>
            <td colspan="2" width="47.61029411764706%">

              <p>
                <br>
              </p>
            </td>
            <td colspan="3" width="37.13235294117647%">

              <p>Director AD</p>
            </td>
            <td width="15.257352941176471%">

              <p>
                <br>
              </p>
            </td>
          </tr>
          <tr>
            <td colspan="2" width="47.61029411764706%">

              <p>
                <br>
              </p>
            </td>
            <td colspan="3" width="37.13235294117647%">

              <p>Jefe de Departamento AD</p>
            </td>
            <td width="15.257352941176471%">

              <p>
                <br></p>
              </td>
            </tr>
          </tbody>
        </table>


        <br clear=all style='mso-special-character:line-break;page-break-before:always'>
        I'm page 2.
      </div>

      <table width="100%">
        <thead style="background-color:#A0A0FF;"><td nowrap>Column A</td><td nowrap>Column B</td><td nowrap>Column C</td></thead>
        <tr><td>A1</td><td>B1</td><td>C1</td></tr>
        <tr><td>A2</td><td>B2 Test with looooong text: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed sapien
          ac tortor porttitor lobortis. Donec velit urna, vulputate eu egestas eu, lacinia non dolor. Cras lacus diam, tempus
          sed ullamcorper a, euismod id nunc. Fusce egestas velit sed est fermentum tempus. Duis sapien dui, consectetur eu
          accumsan id, tristique sit amet ante.</td><td>C2</td></tr>
          <tr><td>A3</td><td>B3</td><td>C3</td></tr>
        </table>

      </body>
      </html>
