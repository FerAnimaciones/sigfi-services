<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('authtoken'))
{
	/**
	*
	*
	* @param null Not have param
	*
	* @return void
	*/
	function authtoken($apache="",$data,$C)
	{
		if (array_key_exists("Authorization",$apache)){
			$decodeinfo="";
			try {
				$decode=JWT::decode($apache["Authorization"],$C->item('jwt_key'));
				if ($decode) {
					$data["authorization"]=true;
					$data["date"]=date("Y-m-d H:i:s");
					$data["apiversion"]=1;
					$decodeinfo=$decode;
				}
			} catch (Exception $e) {
				$data["authorization"]=false;
				$data["api"]="CAPDEM API";
				$data["date"]=date("Y-m-d H:i:s");
				$data["apiversion"]=1;
				$decodeinfo=false;
				//echo 'Excepción capturada: ',  $e->getMessage(), "\n";
			}
		}else {
			$data["authorization"]=false;
			$data["api"]="CAPDEM API";
			$data["date"]=date("Y-m-d H:i:s");
			$data["apiversion"]=1;
			$decodeinfo=false;
		}
		$das["data"]=$data;
		$das["info"]=$decodeinfo;
		return $das;
	}
}
if (!function_exists('checkpermission'))
{
	/**
	*
	*
	* @param null Not have param
	*
	* @return void
	*/
	function checkpermission($autorization,$permisos="",$tipopermitidos="")
	{
		$canacces=false;
		if ($autorization) {
			for ($i=0; $i <count($tipopermitidos); $i++) {
				if ($permisos[0]->tipo==$tipopermitidos[$i]) {
					$canacces=true;
					break;
				}
			}
		}
		return $canacces;
	}
}
if (!function_exists('noauth'))
{
	/**
	*
	*
	* @param null Not have param
	*
	* @return void
	*/
	function noauth($data)
	{
		  $data["authorization"]=false;
			E401();
			return $data;
	}
}
