<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('formatPrice'))
{
	/**
	* This method send json header and CORS
	*
	* @param null Not have param
	*
	* @return void
	*/
	function formatPrice($number, array $options = [])
	{
	    $options = array_replace([
	        'alwaysShowDecimals' => true,
	        'nbDecimals' => 2,
	        'decPoint' => ".",
	        'thousandSep' => "",
	        'moneySymbol' => "€",
	        'moneyFormat' => "vs", // v represents the value, s represents the money symbol
	    ], $options);
	    extract($options);

	    $v = number_format($number, $nbDecimals, $decPoint, $thousandSep);
	    if (false === $alwaysShowDecimals && $nbDecimals > 0) {
	        $p = explode($decPoint, $v);
	        $dec = array_pop($p);
	        if (0 === (int)$dec) {
	            $v = implode('', $p);
	        }
	    }
	    $ret = str_replace([
	        'v',
	        's',
	    ], [
	        $v,
	        $moneySymbol,
	    ], $moneyFormat);
	    return $ret;

	}
}
