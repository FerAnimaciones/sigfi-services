<?php
function SplitName($full_name="") {
  $ListExceptions=array('da', 'de', 'del', 'la', 'las',
  'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa' ,'ladron');
  $ListExceptions2=array('da', 'de', 'del', 'la', 'las',
  'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa' ,'ladron','montes');
  $ListExceptions3=array('a','da', 'de', 'del', 'la', 'las',
  'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa' ,'ladron');
  /* separar el nombre completo en espacios */
  $tokens = explode(' ', trim($full_name));
  /* arreglo donde se guardan las "palabras" del nombre */
  if (count($tokens)<=2) {
    $namesepare["Nombres"]=$tokens[1];
    $namesepare["Paterno"]=$tokens[0];
    $namesepare["Materno"]="";
    return $namesepare;
  }else {
    $names = array();
    /* palabras de apellidos (y nombres) compuetos */
    $special_tokens =null;
    if (strpos($full_name, 'A LA') !== false) {
      $special_tokens =$ListExceptions3;
    }
    if (strpos($full_name, 'MONTES DE') !== false) {
      $special_tokens =$ListExceptions2;
    }else {
      $special_tokens =$ListExceptions;
    }
    $prev = "";
    foreach($tokens as $token) {
      $_token = strtolower($token);
      if(in_array($_token, $special_tokens)) {
        $prev .= "$token ";
      } else {
        $names[] = $prev. $token;
        $prev = "";
      }
    }
    $num_nombres = count($names);
    $nombres = $apellidos = "";
    switch ($num_nombres) {
      case 0:
      $nombres = '';
      break;
      case 1:
      $nombres = $names[0];
      break;
      case 2:
      $nombres    = $names[0];
      $apellidos  = $names[1];
      break;
      case 3:
      $apellidos = $names[0] . ' ' . $names[1];
      $nombres   = $names[2];
      default:
      $apellidos = $names[0] . ' '. $names[1];
      unset($names[0]);
      unset($names[1]);
      $nombres = implode(' ', $names);
      break;
    }
    $namesepare["Nombres"]=$nombres;
    $apellidos  =$apellidos;
    $apellidosf=SplitApellidos($apellidos);
    $namesepare["Paterno"]=$apellidosf["Paterno"];
    $namesepare["Materno"]=$apellidosf["Materno"];
    return $namesepare;
  }
}

function SplitApellidos($apellidos='') {
  $ListExceptions=array('da', 'de', 'del', 'la', 'las',
  'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa' ,'ladron');
  $ListExceptions2=array('da', 'de', 'del', 'la', 'las',
  'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa' ,'ladron','montes');
  $ListExceptions3=array('a','da', 'de', 'del', 'la', 'las',
  'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa' ,'ladron');

  $full_name=$apellidos;
  /* separar el nombre completo en espacios */
  $tokens = explode(' ', trim($full_name));
  /* arreglo donde se guardan las "palabras" del nombre */
  $names = array();
  /* palabras de apellidos (y nombres) compuetos */
  $special_tokens =null;
  if (strpos($full_name, 'A LA') !== false) {
    $special_tokens =$ListExceptions3;
  }
  if (strpos($full_name, 'MONTES DE') !== false) {
    $special_tokens =$ListExceptions2;
  }else {
    $special_tokens =$ListExceptions;
  }
  $prev = "";
  foreach($tokens as $token) {
    $_token = strtolower($token);
    if(in_array($_token, $special_tokens)) {
      $prev .= "$token ";
    } else {
      $names[] = $prev. $token;
      $prev = "";
    }
  }
  $num_nombres = count($names);
  $nombres = $apellidos = "";
  switch ($num_nombres) {
    case 0:
    $nombres = '';
    break;
    case 1:
    $nombres = $names[0];
    break;
    case 2:
    $nombres    = $names[0];
    $apellidos  = $names[1];
    break;
    case 3:
    $apellidos = $names[0] . ' ' . $names[1];
    $nombres   = $names[2];
    default:
    $apellidos = $names[0] . ' '. $names[1];
    unset($names[0]);
    unset($names[1]);

    $nombres = implode(' ', $names);
    break;
  }
  $apellidosr=array();
  $apellidosr["Paterno"]=$nombres;
  $apellidosr["Materno"]=$apellidos;
  return $apellidosr;
}
