<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('HeaderDocx'))
{
	/**
	* This method send header for file output
	*
	* @param null Not have param
	*
	* @return void
	*/
	function HeaderWORD($filename="",$adddate=true)
	{
		$data=$filename;
		if ($adddate) {
			$data.=" - ".date("H:i:s");
		}
		header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
		header("Content-Disposition: attachment; filename=$data.docx");
	}
}

if (!function_exists('getNameInitials'))
{
	/**
	* This method send header for file output
	*
	* @param null Not have param
	*
	* @return void
	*/
	function getNameInitials($name) {

		preg_match_all('#(?<=\s|\b)\pL#u', $name, $res);
		$initials = implode('', $res[0]);

		if (strlen($initials) < 2) {
			$initials = strtoupper(substr($name, 0, 2));
		}

		return strtoupper($initials);
	}
}
