<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('GetMonth'))
{
  /**
  *
  *
  * @param null Not have param
  *
  * @return void
  */
  function GetMonth($fecha)
  {
    try {
      $mes=  date('n',$fecha);
      $anio=  date('Y',$fecha);
      $dia=  date('d',$fecha);
      switch ($mes) {
        case 1:
        $mes = 'Enero';
        break;
        case 2:
        $mes = 'Febrero';
        break;
        case 3:
        $mes = 'Marzo';
        break;
        case 4:
        $mes = 'Abril';
        break;
        case 5:
        $mes = 'Mayo';
        break;
        case 6:
        $mes = 'Junio';
        break;
        case 7:
        $mes = 'Julio';
        break;
        case 8:
        $mes = 'Agosto';
        break;
        case 9:
        $mes = 'Septiembre';
        break;
        case 10:
        $mes = 'Octubre';
        break;
        case 11:
        $mes = 'Noviembre';
        break;
        case 12:
        $mes = 'Diciembre';
        break;
      }
      return $dia." de ".$mes." del ".$anio;
    } catch (Exception $e) {
      return "";
    }
  }
}
if (!function_exists('GetExceldDate'))
{
  /**
  *
  *
  * @param null Not have param
  *
  * @return void
  */
  function GetExceldDate($excel_date)
  {
    if($excel_date!=""){
      if (is_numeric($excel_date)) {
        $unix_date = ($excel_date - 25569) * 86400;
        $excel_date = 25569 + ($unix_date / 86400);
        $unix_date = ($excel_date - 25569) * 86400;
        return gmdate("Y-m-d", $unix_date);
      }else{
        return null;
      }
    }else{
      return null;
    }
  }
}
if (!function_exists('getFormatDate'))
{
  /**
  *
  *
  * @param null Not have param
  *
  * @return void
  */
  function getFormatDate($date="")
  {
    if($date==""){
      return "";
    }else{
      return date("d/m/Y", strtotime($date));
    }
  }
}
