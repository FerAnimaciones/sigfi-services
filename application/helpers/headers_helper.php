<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('HeaderJson'))
{
	/**
	* This method send json header and CORS
	*
	* @param null Not have param
	*
	* @return void
	*/
	function HeaderJson($value="")
	{
		header('Content-Type: application/json; charset=utf-8');
			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Credentials: true');
			header("Access-Control-Allow-Methods: GET, OPTIONS,POST");
			header("Access-Control-Allow-Headers: Origin, Authorization, X-Requested-With, Content-Type, Accept,Accept-Encoding , Content-Length,,Authorization");
	}
}
if (!function_exists('HeaderAuth'))
{
	/**
	* This method send json header with Auth
	*
	* @param null Not have param
	*
	* @return void
	*/
	function HeaderAuth($value="")
	{
		header('Access-Control-Allow-Headers: Authorization');
	}
}

if (!function_exists('E404'))
{
	/**
	* This method send  404 Not Found header.
	*
	* @param null Not have param
	*
	* @return void
	*/
	function E404($value="")
	{
		header("HTTP/1.1 404 Not Found");
	}
}
if (!function_exists('E401PDF'))
{
	/**
	* This method send  404 Not Found header.
	*
	* @param null Not have param
	*
	* @return void
	*/
	function E401PDF($value="")
	{
	 header("HTTP/1.1 401 Unauthorized");
	 $data["authorization"]=false;
	 $data["apiversion"]=1;
	 $data["date"]=date("Y-m-d H:i:s");
	 echo json_encode($data);
	}
}
if (!function_exists('NoAuthPost'))
{
	/**
	* This method send  404 Not Found header.
	*
	* @param null Not have param
	*
	* @return void
	*/
	function NoAuthPost($ci="")
	{
		if($ci->input->method()=="post") {
			E401();
		}
	}
}
if (!function_exists('NoAuthGet'))
{
	/**
	* This method send  404 Not Found header.
	*
	* @param null Not have param
	*
	* @return void
	*/
	function NoAuthGet($ci="")
	{
		if($ci->input->method()=="get") {
			E401();
		}
	}
}
